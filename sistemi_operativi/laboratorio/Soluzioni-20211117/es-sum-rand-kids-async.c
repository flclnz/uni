#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

#define NUM_KIDS  20

void handle_sigchild(int);

int main (int argc, char * argv[]) {
	struct sigaction sa;
	int i, status, num;

	/* Setting up SIGCHLD handler */  
	sa.sa_handler = handle_sigchild;
	sa.sa_flags = 0;
	sigemptyset(&sa.sa_mask);
	sigaction(SIGCHLD, &sa, NULL);

	/* Ciclo per la creazione dei processi figlio */
	for (i=0; i<NUM_KIDS; i++) {
		switch (fork()) {
		case -1:
			/* Handle error */
			fprintf(stderr, "%s, %d: Errore (%d) nella fork\n",
				__FILE__, __LINE__, errno);
			exit(EXIT_FAILURE);
		case 0:
			/* CHILD CODE */
			/*
			 * Inizializza il seed con il PID del figlio.
			 * In questo modo, la generazione di numeri
			 * casuali sara` diversa per ogni figlio.
			 */
			srand(getpid());
			num = rand()%6+1;
			if (num == getpid() % 6) {
				raise(SIGTERM);
			}
			printf("PID=%d, num=%d\n", getpid(), num);
			exit(num);
			break;
		default:
			/* PARENT CODE: nothing here */
			break;
		}
	}

	/* Just do not terminate here. The handler will */
	while (1)
		pause();
}

void handle_sigchild(int signum) {
	static int sum_exit=0;
	pid_t child_pid;
	int status;
	int errno_saved;
	
	errno_saved = errno;           /* save errno as we may change it */
	
	while (child_pid = waitpid(WAIT_ANY, &status, WNOHANG),
	       child_pid != 0 && child_pid != -1) {
		
		/* Checking the status */
		if (WIFEXITED(status)) {
			printf("PARENT: child %d exited with status %d\n",
			       child_pid, WEXITSTATUS(status));
			sum_exit += WEXITSTATUS(status);
		}
		if (WIFSIGNALED(status)) {
			printf("PARENT: child %d terminated by the signal %d\n",
			       child_pid, WTERMSIG(status));
		}
	}

	/* Terminating when no more child processes */
	if (child_pid == -1 && errno == ECHILD) {
		printf("Sum of exit statuses is\n%u\n", sum_exit);
		exit(0);
	}

	errno = errno_saved;           /* restoring errno */
}
