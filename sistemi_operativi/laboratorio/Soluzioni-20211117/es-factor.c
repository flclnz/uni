/*
 * Segue lo svolgimento del seguente esercizio:
 *
 * Si scriva un programma che legga  un unsigned int da stdin e scriva
 * la sua fattorizzazione in fattori primi nel seguente formato
 *
 * 126 = 2^1*3^2*7^1
 *
 * se la macro CSV non è definita,  mentre se la macro CSV è definita,
 * il formato * adottato è il seguente
 * 
 * 2,1
 * 3,2
 * 7,1
 *
 * ovvero  su ogni  riga vengono  scritti  base ed  esponente di  ogni
 * fattore. Si  gestisca il caso  in cui  il numero inserito  eccede i
 * limiti dovuti all’implementazione.
 * 
 * IMPORTANTE:  Per  massimizzare  l'apprendimento, si  raccomanda  di
 * leggere  la  soluzione  proposta  soltanto *dopo*  aver  provato  a
 * risolvere il problema individualmente.
 */
#include <stdio.h>
#include <stdlib.h>

#ifndef STR_LEN
#define STR_LEN 25
#endif

#ifndef MAX_FACTORS
#define MAX_FACTORS 15
/*
 * MAX_FACTORS=15 e` sufficiente per tutti i numeri fino a
 *
 * 2^63-1
 *
 * (come mai?)
 */
#endif


int main()
{
	char s[STR_LEN];   /* stringa inserita */
	unsigned long num, in_num, cur_div, i, factors[MAX_FACTORS];
	char pos_div, exponents[MAX_FACTORS];

	/* Leggi la stringa e convertila in numero */
	printf("Inserire il numero da fattorizzare\n");
	fgets(s, STR_LEN, stdin);
	/* Converte stringa in long */
	in_num = num = atol(s);

	/* Inizializza i fattori a zero */
	for(i=0; i<MAX_FACTORS; i++) {
		factors[i] =  0;
	}
	if (num <= 1) {
		printf("Il numero da inserire deve essere maggiore di 1\n");
		return -1;
	}
	
	/* 
	 * La fattorizzazione viene fatta facendo divisioni successive
	 * del numero num per divisori sempre crescenti
	 */
	cur_div = 2;   /* divisore iniziale */
	pos_div = 0;   /* posizione divisore in vettori */
	while (num >= 2) {
		if (!(num % cur_div)) {
			/* cur_div divide num */
			factors[pos_div] = cur_div;
			exponents[pos_div] = 1;
			num /= cur_div;
			/* Dividi per cur_div finche' possibile */
			while (!(num % cur_div)) {
				exponents[pos_div]++;
				num /= cur_div;
			}
			pos_div++;
		}
		cur_div++;
	}
	/* Stampa come richiesto */
#ifdef CSV
	for(pos_div = 0; factors[pos_div]; pos_div++) {
		/* 
		 * Quando la printf ha tanti parametri, talvolta viene
		 * spezzata su piu` righe
		 */
		printf("%li,%i\n",
		       factors[pos_div],
		       exponents[pos_div]);
	}
#else
	printf("%li=%li^%i",in_num, factors[0], exponents[0]);
	for(pos_div = 1; factors[pos_div]; pos_div++) {

		printf("*%li^%i",
		       factors[pos_div],
		       exponents[pos_div]);
	}
	printf("\n");
#endif
	return 0;
}
