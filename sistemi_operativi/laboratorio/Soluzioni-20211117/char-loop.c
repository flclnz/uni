#define _GNU_SOURCE
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#define CHAR_FIRST '!'
#define CHAR_LAST '~'

#define IS_PRINTABLE(c)  ((c) >= CHAR_FIRST && (c) <= CHAR_LAST)

/*
 * Variabile globale per essere vista sia all'interno dell'handler che
 * nel main.
 */
unsigned char c='X'; /* Inizializzazione */

/*
 * Dichiarazione dell'handler
 */
void handle_signal(int signal);


int main(int argc, char * argv[])
{
	struct sigaction sa;
	sigset_t  my_mask;

	/*
	 * Impostazione dell'handler per SIGINT
	 */
	sa.sa_handler = handle_signal;  /* La funzione */
	sa.sa_flags = 0; 	        /* Nessun flag particolare */
	sigemptyset(&my_mask);          /* Maschera vuota */
	sa.sa_mask = my_mask;
	if (sigaction(SIGINT, &sa, NULL) == -1) {
		fprintf(stderr, "%s, %d: Errore nell'impostazione dell'handler (%d, %s)\n",
			__FILE__, __LINE__, errno, strerror(errno));
		exit(1);
	}

	if (argc > 1) {
		/* 
		 * argv[1] e` il primo argomento passato a riga di
		 * comando.  argv[1][0] e` il primo carattere del
		 * primo argomento, come richiesto
		 */
		c = argv[1][0];
		if (!IS_PRINTABLE(c)) {
			fprintf(stderr, "PID %d: passato carattere non stampabile, cod ASCII (dec) = %d\n", getpid(), c);
			exit(1);
		}
	}
	
	/*
	 * Ciclo forever, che incrementa c, assicurandosi che il
	 * valore sia corretto
	 */
	while (1) {
		c++;
		if (c > CHAR_LAST) {
			c = CHAR_FIRST;
		}
	}
}

/*
 * Corpo dell'handler
 */

void handle_signal(int signum)
{
	if (signum == SIGINT) {
		/*
		printf("Carattere corrente \"%c\" (dec=%u, hex=%X)\n",
		       c, c, c);
		*/
		exit(c);
	}
}
