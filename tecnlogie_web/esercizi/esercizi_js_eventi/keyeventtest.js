var i = 0;

window.onload = function() {
  $("#target").on("keydown",  function(event) {eventStatus(event, "#temp");});
  $("#target").on("keyup", function(event) {eventStatus(event, "#temp2");});
  $("#target").on("keypress", function(event) {eventStatus(event, "#temp3");}); 
};

function eventStatus(event, id) {
  var s = "event type="+event.type+"<br>";
  if (undefined !== event.keyCode) {
    s += "keyCode=" + event.keyCode + "<br/>";
  }
  if (undefined !== event.charCode) {
    s += "charCode=" + event.charCode + "<br/>";
  }
  if (undefined !== event.which) {
    s += "which=" + event.which + "<br/>";
  }
  if (undefined !== event.code) {
    s += "code=" + event.code + "<br/>";
  }
  s += "altKey=" + event.altKey + ", ctrlKey=" + event.ctrlKey + 
  		", shiftKey=" + event.shiftKey + "<br/><br/>";
  s += i++;
  
  $(id).html(s);
}
