var RETURN_KEY = 13;
var KEY_SPACE = 32;
var KEY_BACKSPACE = 8;
var KEY_LEFT = 37;
var KEY_RIGHT = 39;
var KEY_TAB = 9;
var ALLOWED = [RETURN_KEY, KEY_SPACE, KEY_BACKSPACE, KEY_LEFT, KEY_RIGHT, KEY_TAB];  
// keycodes for space, backspace and others are allowed

$(function() {
  $("#zip").on("keydown", zipKeyDown);
});



// Called when a key is pressed on the zip code field.
// Disallows non-numeric characters from being typed.
function zipKeyDown(event) {
  var zero = "0".charCodeAt(0);
  var nine = "9".charCodeAt(0);
  var key = event.keyCode; 
    
  if ((key < zero || key > nine) && ALLOWED.indexOf(key) < 0) {
      event.preventDefault();
  }
}
