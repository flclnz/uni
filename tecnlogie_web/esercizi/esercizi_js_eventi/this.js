var beerCount = 99;

$(function() {
  $("input[name='ducks']").change(processDucks);
});

function processDucks() {
    $("#checkedduck").html($(this).val() + " is checked!");
    // alternatively:
    /* 
    var duck = $("input[name='ducks']:checked").val();
    $("#checkedduck").html(duck + " is checked!");
    */
}
