var RETURN_KEY = 13;
var KEY_SPACE = 32;
var KEY_BACKSPACE = 8;
var KEY_LEFT = 37;
var KEY_RIGHT = 39;
var KEY_TAB = 9;
var ALLOWED = [RETURN_KEY, KEY_SPACE, KEY_BACKSPACE, KEY_LEFT, KEY_RIGHT, KEY_TAB];   
// keycodes for space, backspace, and others are allowed
  
$(function() {
  $("#zip").on("keydown", zipKeyDown);
  $("#zipform").on("submit", zipFormSubmit);
});

// Called when a key is pressed on the zip code field.
// Disallows non-numeric characters from being typed.
function zipKeyDown(event) {
    var zero = "0".charCodeAt(0);
    var nine = "9".charCodeAt(0);
    var key = event.keyCode; 
    if ((key < zero || key > nine) && ALLOWED.indexOf(key) < 0) {
        event.preventDefault();
    }
}

// Called when the user tries to submit the form.
function zipFormSubmit(event) {
  if ($("#zip").val().length != 5) {
    // bad ZIP code; stop form from submitting and show error msg
    $("#ziperror").html("ZIP code must be 5 characters.");
    $("#zip").addClass("badformdata");
    event.preventDefault();
  }
}
