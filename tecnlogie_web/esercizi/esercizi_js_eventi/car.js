$(function() {
  $("#car").on("change", carChange);
});

// Enables hybrid option when Toyota is chosen as car maker
function carChange(event) {
  $("#selectedCar").text(this.value);
  $("#hybrid").attr("disabled", (this.value != "Toyota"));
}
