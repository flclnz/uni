$(document).ready(function() {
    $( "input" ).on("change",function() {
        var myinput = $( this );
        $( "p" ).html(
            ".attr( \"checked\" ): <b>" + myinput.attr( "checked" ) + "</b><br>" +
            ".prop( \"checked\" ): <b>" + myinput.prop( "checked" ) + "</b><br>" +
            ".is( \":checked\" ): <b>" + myinput.is( ":checked" ) + "</b>" );
    });
});