var beerCount = 99;

$(function() {
  $("#beercounter").on("mouseover", countBeers);
});

function countBeers() {
  $("#beercounter").html((--beerCount) + " bottles of beer");
}
