$(function() {
  $("#status").on("mousemove", showCoordinates);
  $("#status").on("mousedown", showCoordinates);
  $("#status").on("mouseup", showCoordinates);
    // the following is an alternative code
    /*
    $("#status").on({
        onmousemove : showCoordinates,
        onmousedown : showCoordinates,
        onmouseup   : showCoordinates
    })
    */
});

// Called when any of several mouse events occurs on the status area.
function showCoordinates(event) {
  $("#status").html("A " + event.type + " event occurred at (" +
      event.pageX + ", " + event.pageY + ")");
}
