$(function() {
    $.ajax({
        url: "books.php", // test also with "categories.xml" 
        type: "GET",
        datatype: "xml",
        success: showCategories,
        error: ajaxFailed     
        }
    );
});

function showCategories(xml) {
    // clear out the list of categories
    $("#categories").empty();
    

    // add all categories from the XML to the page's bulleted list
   $(xml).find("category").each(function(){
       categoryName = $(this).first().text();
       // create a new <li> tag and add it to the page
       var li = $('<li></li>');
       li.click(categoryClick);
       li.text(categoryName);
       $("#categories").append(li);   
   });
}

function categoryClick() {
    $.ajax({
        url: "books.php", 
        type: "GET",
        data: "category= "+this.innerHTML,
        datatype: "xml",
        success: showBooks,
        error: ajaxFailed,
        }
    );
}

function showBooks(xml) {
    // clear out the list of categories
   $("#books").empty();
    
    // add all books from the XML to the page's bulleted list
    $(xml).find("book").each(function(){
        
        var title  = $(this).find("title").first().text();
        var author =$(this).find("author").first().text();
        var year = $(this).attr("year");
        
        var li = $('<li></li>');
        li.html(title + ", by " + author + " (" + year + ")");
        $("#books").append(li);
    });
}

function ajaxFailed(e) {
	var errorMessage = "Error making Ajax request:\n\n";
		
	errorMessage += "Server status:\n" + e.status + " " + e.statusText + 
		                "\n\nServer response text:\n" + e.responseText;
    alert(errorMessage);
}
