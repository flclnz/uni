$(function(){
var $btn = $('#request');
var $bio = $('#bio');
 
$btn.on('click', function() {
  $(this).hide();
  $bio.load('einstein.txt', completeFunction);
});
 
function completeFunction(responseText, textStatus, request) {
  $bio.css('border', '1px solid #e8e8e8');
  if(textStatus == 'error') {
      console.log('An error occurred during your request: ' +  request.status + ' ' + request.statusText);
      // if you want to display the message in place of Einstein's bio, uncomment below
      /*
      $bio.text('An error occurred during your request: ' +  request.status + ' ' + request.statusText);
      */
  }
}
});