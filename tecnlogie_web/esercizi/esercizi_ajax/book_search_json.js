$(function() {
    $.ajax({
        url: "books_json.php", // test also with "categories.xml" 
        type: "GET",
        datatype: "json",
        success: showCategories,
        error: ajaxFailed     
        }
    );
});

function showCategories(json) {
    // clear out the list of categories
    $("#categories").empty();
    
    // add all categories from the JSON to the page's bulleted list
    json.categories.forEach(function(item){
       // create a new <li> tag and add it to the page
       var li = $('<li></li>');
       li.click(categoryClick);
       li.text(item.category);
       $("#categories").append(li);   
   });
}

function categoryClick() {
    $.ajax({
        url: "books_json.php", 
        type: "GET",
        data: "category= "+this.innerHTML,
        datatype: "json",
        success: showBooks,
        error: ajaxFailed,
        }
    );
}

function showBooks(json) {
    // clear out the list of categories
   $("#books").empty();
    
    // add all books from the JSON to the page's bulleted list
    json.books.forEach(function(item){
        
        var li = $('<li></li>');
        li.html(item.title + ", by " + item.author + " (" + item.year + ")");
        $("#books").append(li);
    });
}

function ajaxFailed(e) {
	var errorMessage = "Error making Ajax request:\n\n";
		
	errorMessage += "Server status:\n" + e.status + " " + e.statusText + 
		                "\n\nServer response text:\n" + e.responseText;
    alert(errorMessage);
}
