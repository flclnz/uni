$(function() {
    $.ajax({
        url: "books.php", // test also with "categories.xml" 
        type: "GET",
        datatype: "xml",
        success: showCategories,
        error: ajaxFailed     
        }
    );
});

function showCategories(xml) {
    // clear out the list of categories
    $("#categories").empty();
    

    // add all categories from the XML to the page's bulleted list
    var categories = xml.getElementsByTagName("category");
    for (var i = 0; i < categories.length; i++) {
        var categoryName = categories[i].firstChild.nodeValue;

        // create a new <li> tag and add it to the page
        var li = document.createElement("li");
        li.innerHTML = categoryName;
        li.onclick = categoryClick;
        $("#categories").append(li);
    }
}

function categoryClick() {
    $.ajax({
        url: "books.php", 
        type: "GET",
        data: "category= "+this.innerHTML,
        datatype: "xml",
        success: showBooks,
        error: ajaxFailed,
        }
    );
}

function showBooks(xml) {
    // clear out the list of categories
   $("#books").empty();
    
    // add all books from the XML to the page's bulleted list
    var books = xml.getElementsByTagName("book");
    for (var i = 0; i < books.length; i++) {
        var titleNode  = books[i].getElementsByTagName("title")[0];
        var authorNode = books[i].getElementsByTagName("author")[0];
        var title  = titleNode.firstChild.nodeValue;
        var author = authorNode.firstChild.nodeValue;
        var year = books[i].getAttribute("year");
        
        var li = document.createElement("li");
        li.innerHTML = title + ", by " + author + " (" + year + ")";
        $("#books").append(li);
    }
}

function ajaxFailed(e) {
	var errorMessage = "Error making Ajax request:\n\n";
		
	errorMessage += "Server status:\n" + e.status + " " + e.statusText + 
		                "\n\nServer response text:\n" + e.responseText;
    alert(errorMessage);
}