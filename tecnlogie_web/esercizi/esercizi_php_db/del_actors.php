<!DOCTYPE html>
<html lang="en">
  <head>
    <title>PHP and MySQL are Friends</title>
  </head>

  <body>
<h1>Actors whose names begin with &quot;Del&quot;</h1>

<ul>
  <?php
    # connect to the database and ask it for all movies
    $connectstr = "mysql:dbname=imdb_small;host=localhost:3306";
    try {
      $db = new PDO($connectstr, "root", "");
    } catch(PDOException $ex){
      die('Could not connect: ' . $ex->getMessage());
    }

    $results = $db->query("SELECT first_name, last_name
                            FROM actors
                            WHERE last_name LIKE 'Del%';");
    while ($row = $results->fetch()) {
      ?>

      <li>
        First name: <?= $row["first_name"] ?>,
        Last name: <?= $row["last_name"] ?>
      </li>

      <?php
    }
  ?>
</ul>

	</body>
</html>
