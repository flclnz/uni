<!DOCTYPE html>
<html lang="en">
  <head>
    <title>PHP and MySQL can deal with errors</title>
  </head>

  <body>
  	<h1>Movies Made in 2000</h1>

<?php
try {
  $db = new PDO("mysql:dbname=imdb_small", "jessica", "2thumbsup");
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $rows = $db->query("SEELECT * FROM movies WHERE year = 2000");
  foreach ($rows as $row) {
    ?>
    <li> <?= $row["name"] ?> got a score of <?= $row["rank"] ?> </li>
    <?php
  }
} catch (PDOException $ex) {
  ?>
  <p>Sorry, a database error occurred. Please try again later.</p>
  <p>(Error details: (<em><?= $ex->getMessage() ?></em>)
  <?php
}
?>
  </body>
</html>
