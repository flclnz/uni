<!DOCTYPE html>
<html lang="en">
  <head>
    <title>PHP and MySQL are Friends</title>
  </head>

  <body>
  	<h1>Movies Made in 2000</h1>

  	<ul>
    <?php
		# connect to the database and ask it for all movies
		$connectstr = "mysql:dbname=imdb_small;host=localhost:3306";
		try {
			$db = new PDO($connectstr, "root", "");
		} catch(PDOException $ex){
			die('Could not connect: ' . $ex->getMessage());
		}
		try {
			# prepare and execute a SQL query on the database
			$results = $db->prepare("SELECT * FROM movies WHERE year = 2000");
			$results->execute();

			$results1 = $db->prepare("SELECT * FROM movies WHERE year = 1999");
		} catch(PDOException $ex){
			die("Query failed: " . $ex->getMessage());
		}
		# loop through each result
		while ($row = $results->fetch()) {
    ?>

    <li><?= $row["name"] ?></li>

    <?php
		}
	?>
	</ul>
	<h1>Movies Made in 1999</h1>
	<ul>
	<?php
		# clean up resources
		$results->closeCursor();
		$db=null;
		
		$results1->execute();
		# loop through each result
		while ($row = $results1->fetch()) {
    ?>

    <li><?= $row["name"] ?></li>

    <?php
		}
	?>

    </ul>
  </body>
</html>

