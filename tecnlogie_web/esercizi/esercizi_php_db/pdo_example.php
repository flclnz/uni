<!DOCTYPE html>
<html lang="en">
  <head>
    <title>PHP and MySQL can deal with errors</title>
  </head>

  <body>
    <h1>Movies Made in 2000</h1>
    <ul>
      <?php
      $db = new PDO("mysql:host=localhost;dbname=imdb_small", "siskel", "2thumbsup");
      $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      # var_dump($db);
      
      $query = "SELECT * FROM movies WHERE year = 2000";
      # var_dump($query);
      
      $rows = $db->query($query);
      # var_dump($rows);
      
      if ($rows === FALSE) {
        ?>
      <p>Database error:
        <?php var_dump($db->errorInfo()); ?>
        <?php var_dump($db->errorCode()); ?>
      </p>
      <?php
      } else {
        foreach ($rows as $row) {
          ?>
          <li> <?= $row["name"] ?> got a score of <?= $row["rank"] ?> </li>
          <?php
        }
      }
      ?>
    </ul>
    </body>
</html>
