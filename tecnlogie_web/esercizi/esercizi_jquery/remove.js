$(function(){
    $("#btn").click(removeList);
});

function removeList () {
    $("li").each(function(){
        if($(this).html().indexOf("Stone") >= 0) {
            $(this).remove();
        }
    });
} 
  
// same function but with vanilla js
/* 
function removeList () {
    var bullets = document.getElementsByTagName("li");
	for (var i = 0; i < bullets.length; i++) {
		if (bullets[i].innerHTML.indexOf("Stone") >= 0)                   
             {
	        bullets[i].parentNode.removeChild(bullets[i]);
	      }
	}
} 
*/	      
