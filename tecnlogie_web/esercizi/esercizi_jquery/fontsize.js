
$(document).ready(function(){
    //get inital font size
    getSize();
    
    $( "#up" ).click(function() {
        // parse font size, if less than 50 increase font size
        if ((size + 2) <= 50) {
            $( "h1" ).css( "font-size", (size+2)+"px" );
           // $( "h1" ).css( "font-size", "+=2" );
            $( "#font-size" ).text(  size += 2 );
        }
    });

    $( "#down" ).click(function() {
        if ((size - 2) >= 12) {
        $( "h1" ).css( "font-size", "-=2" );
        $( "#font-size" ).text(  size -= 2  );
      }
    });
    
});

	
// When + or - buttons are clicked the font size of the h1 is increased/decreased by 2
// The max is set to 50px for this demo, the min is set by min font in the user's style sheet

function getSize() {
  size = $( "h1" ).css( "font-size" );
  size = parseInt(size, 10);
  $( "#font-size" ).text(  size  );
}




