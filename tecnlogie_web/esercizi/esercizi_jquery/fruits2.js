
$(document).ready(function(){
  // change some elements' background color with find
    $('#apples').find('[title="yummy!"]').css({"background-color":"red"});
    // -> [li#mutsu, …]
    $('#apples').find( 'p#saying >li[title="yummy!"]').css({"background-color":"green"});
    // ->	 
    $('#apples').find('[title="disgusting!"]').css({"background-color":"blue"});
});

$('#apples').find('[title="yummy!"]');
// -> [li#mutsu, …]
$('#apples').find( 'p#saying >li[title="yummy!"]');
// ->	 
$('#apples').find('[title="disgusting!"]');