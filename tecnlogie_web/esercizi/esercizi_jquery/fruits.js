
$(document).ready(function(){
  // change some elements' background color with find
    $('#fruits').find('.yummy').css({"background-color":"yellow"});
    // -> [li#mutsu, li#mcintosh, li#exotic]
    $('#exotic').find('.yummy').css({"background-color":"red"});
    // ->	 
});

