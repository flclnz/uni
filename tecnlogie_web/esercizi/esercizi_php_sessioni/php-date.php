<?php
$one_hour_from_now = time() + 60 * 60;
$datestr = date("l g:i A", $one_hour_from_now);
?>

<p>Congratulations, lucky winner!
   You have been randomly selected to receive a $10-off coupon!
   Your coupon expires in one hour: <?= $datestr ?>.
</p>
