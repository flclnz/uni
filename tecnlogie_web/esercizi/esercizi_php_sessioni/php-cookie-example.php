<?php
setcookie("heroname", "Lancelot");              # session cookies
setcookie("quest", "To seek the Holy Grail");

$expire = time() + (60 * 60 * 24 * 7);          # a persistent cookie, expiring
setcookie("favoritecolor", "blue", $expire);    # one week from now

setcookie("swallowvelocity", "", -1);           # deleting a cookie

if (isset($_COOKIE["heroname"]) && isset($_COOKIE["quest"])) {
  ?>
  <p>Welcome back to this great site, <?= $_COOKIE["heroname"] ?>!
     We wish you luck on your quest: "<?= $_COOKIE["quest"] ?>"!</p>

  <?php
} else {
  ?>
  <p>Welcome, new user! Welcome to our site.
     What is your name? ...</p>
<?php } ?>
