<!DOCTYPE html>

<html>
<head>

<style>
table, td {
    border: 1px solid black; 
    border-collapse: collapse; 
    padding-left: 1.2em;
    padding-right: 1.2em;
    font-family: Verdana, Geneva, Tahoma, sans-serif;
    text-align: center;
    background-color: yellow;
}
</style>

</head>

<body>

    <table>
        <?php
            for($i = 1; $i < 8; $i++){
        ?>
            <tr>
            <?php
                for($x = 1; $x < 8; $x++) {
            ?>
                    <td> <?= $i*$x ?> </td>
            <?php
                }
            ?>
            </tr>
        <?php
            }   
        ?>
    </table>
</body>

</html>