<?php
$mese = "August";
if ($mese == date('F', time())) {
    ?>
    È agosto, e c’è davvero caldo.
    <?php
} else {
    ?>
   Non è agosto, per lo meno non è il periodo più caldo dell’anno.
<?php
}
?>