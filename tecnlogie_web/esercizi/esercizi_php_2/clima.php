<!DOCTYPE html>
<html lang="it">
    <head>
        <meta charset="UTF-8">
        <title>Clima</title>
    </head>
    <body>
        <h1>Non ci sono più le mezze stagioni!</h1>
        <p>
            <?php
            $arr = array("pioggia", "sole", "nuvole", "grandine", "nebbia", 
            "neve", "vento");
            print "Abbiamo avuto tutti i tipi di clima questo mese. 
            All’inizio del mese abbiamo avuto $arr[5] e $arr[6]. 
            Poi è arrivato il $arr[1] con un po’ di $arr[2] ed infine un po’ di 
            $arr[0]. Almeno non abbiamo avuto $arr[3] e $arr[4]."
            ?>
        </p>
      
        <?php 
        $condizioni = array("pioggia","sole","nuvole","grandine","nebbia",
        "neve","vento");
        ?>
        <p>Abbiamo avuto tutti i tipi di clima questo mese. All’inizio
        del mese abbiamo avuto <?= $condizioni[5] ?> e 
        <?= $condizioni[6] ?>. 
        Poi è arrivato il <?= $condizioni[1] ?> con un po’ 
        di <?= $condizioni[2] ?> ed infine un po’ 
        di <?= $condizioni[0] ?>. Almeno non abbiamo avuto 
        <?= $condizioni[3] ?> e <?= $condizioni[4] ?>.
        </p>

    </body>


</html>