<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Città</title>
    </head>
    <body>
    <h1>Le città più grandi del mondo</h1>
    <h2>Non ordinate</h2>
    <?php
        $cities = array("Tokyo", "Mexico City", "New York City", 
        "Mumbai", "Seoul", "Shanghai", "Lagos", "Buenos Aires", "Cairo", 
        "London");

        print implode(", ", $cities);
    ?>
    <h2>Ordinate</h2>
    <?php
        asort($cities);
    ?>
    <ul>
        <?php
        foreach($cities as $city) {
        ?>

        <li><?= $city ?></li>
        <?php
        } 
        ?>

    </ul>
    <h2>Altre città</h2>
    <?php
        array_push($cities,"Los Angeles", "Calcutta", "Osaka", "Beijing");
        /*$cities[] = "Los Angeles";
        $cities[] = "Calcutta";
        $cities[] = "Osaka"; 
        $cities[] = "Beijing";*/
        asort($cities);
    ?>
    <ul>
        <?php
        foreach($cities as $city) {
        ?>

        <li><?= $city ?></li>
        <?php
        } 
        ?>
    </ul>

    </body>
</html>