<?php
if ($_SERVER["REQUEST_METHOD"] == "GET") {
	# GET request; parameters are processed and the result is generated
	$base = $_REQUEST["base"];
	$exp = $_REQUEST["exponent"];
	$result = pow($base, $exp);
	
	$arr = array('base'=>$base, 'exponent'=>$exp, 'result'=>$result);

	# uncomment below to set content-type to plain text in the HTTP response message
	#header("Content-Type: text/plain; charset=UTF-8");
	
	# set content-type to application/json in the HTTP response message
	header("Content-Type: application/json; charset=UTF-8");

	# write a stringfied array that is encoded as json
	echo json_encode($arr);

} elseif ($_SERVER["REQUEST_METHOD"] == "POST") {
	# POST request; user is sent back to the original form
	header('Location: ./power.shtml');
	exit;
  }
?>