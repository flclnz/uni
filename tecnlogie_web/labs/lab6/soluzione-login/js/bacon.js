$(function() {

    $("#errMsg").hide();

    // check login status, and if logged, gets the user name
    $.post({
        url: "../php/checkLogin.php", 
        datatype: "json",
        success: completeIndex,
        error: goToLogin,
    });  

    // dismiss open session and redirect to login.php when logout button is pressed
    $("#sumbitLogout").on("click",function(){
        $.get({
            url: "../php/logout.php",
            success: function() {
                $(window.location).attr('href', 'login.php');
            }
        })
    });
  
    // get selected actor's movie list played with Kevin Bacon 
    // when corresponding submit button is pressed
    $("#searchkevin input[name='submit']").on("click",function() {
        $.get({
            url: "../php/getMovieList.php", 
            data: "firstname="+$("#searchkevin input[name='firstname']").val()
                +"&lastname="+$("#searchkevin input[name='lastname']").val(),
            datatype: "json",
            success: printMovieList
        });
        $("input[name='firstname']").val("");
        $("input[name='lastname']").val("");
    });
        
    // get selected actor's movie list when corresponding submit
    // button is pressed
    $("#searchall input[name='submit']").on("click",function() {
        $.get({
            url: "../php/getMovieList.php", 
            datatype: "json",
            data: "firstname="+$("#searchall input[name='firstname']").val()
                +"&lastname="+$("#searchall input[name='lastname']").val()
                +"&all=true",   // also all=true argument is passed to getMovieList.php
            success: printMovieList
        });
        $("input[name='firstname']").val("");
        $("input[name='lastname']").val("");
    });
});

/*  -----------------------
    Show results functions
    ----------------------- */

function printMovieList(json){
    // remove every row in the table containing previous results
    $(".rowMovie").remove();

    // remove kevin picture to list movies
    $("#kevinphoto").remove();
    
    // print error messages if the actor or actor's movies have not been found
    if(json.errMsg){
        $("#errMsg").text(json.errMsg);
    }
    else {
        $("#errMsg").text("");
        $("#errMsg").hide();
        printResultsHeader();
        // insert actor's name information
        $("#firstN").text(json.firstname);
        $("#lastN").text(json.lastname); 
        var num =1;
        // print movies list
        json.movies.forEach(function(item){
            // create a new <tr> tag and add it to the table
            var tr = $('<tr></tr>');
            // add a class to every row that is added
            tr.addClass("rowMovie");
            // create three new <td> cells with movie's information
            var td1 = $('<td></td>');
            td1.addClass("ext");
            td1.text(num++);
            var td2 = $('<td></td>');
            td2.text(item.name);
            var td3 = $('<td></td>');
            td3.addClass("ext");
            td3.text(item.year);
            tr.append(td1,td2,td3);
            $("#list").append(tr);   
        });
        // scroll smoothly to the top
        $("html, body").animate({ scrollTop: 0 }, "slow");
    } 
}   // end printMovieList

function printResultsHeader() {
    // add to the index's DOM the header of the results set table
    // WARNING: this function implemented just for fun;
    //          it is better to leave html skeleton in index.php and
    //          show (hide) #results when needed (not needed)
    var h2 = $("<h2></h2>");
    h2.html("Results for ");
    h2.append($("<span></span>").attr("id","firstN"));
    h2.append($("<span></span>").attr("id","lastN"));

    var table = $("<table></table>").attr("id","list");
    table.append($("<caption></caption").html("All Films below"));

    var tr = $("<tr></tr>");
    tr.append(
        $("<th></th>").html("#"),
        $("<th></th>").html("Name"),
        $("<th></th>").html("Year"),
    );
    table.append($("<thead></thead>").append(tr));
    table.append($("<tbody></tbodY"));

    $("#results").append(h2,table);

} // function printResultsHeader

/*  -----------------------
    Manage log in functions
    ----------------------- */

function completeIndex(json){
    // if session has been set and username is found, then we welcome the user and 
    // we add logout button to each page
    if(json.isLogged){
        $("#username").text(json.name);
        $("#kevinphoto").attr("src","../img/kevin_bacon.jpg");
    } else {
        // if logged is false, then the session is not active and the user needs to log in
        $(window.location).attr('href', 'login.php');
    }
}   // end printName

function goToLogin() {
    // if something goes wrong, go to login page
    $(window.location).attr('href', 'login.php');
}

