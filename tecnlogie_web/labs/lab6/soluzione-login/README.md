# Soluzione lab06 (con login)

In questa cartella trovate tutti i file relativi alla nostra proposta di soluzione del compito lab06 con la funzionalità opzionale di accesso al servizio regolato da login/logout.

Per eseguire correttamente questo sito dovete modificare il database imdb (o imdb_small) introducendo una tabella addizionale `users`. Potete usare il file  `.sql/users.sql`. Contiene del codice sql che potete eseguire da phpMyAdmin (grazie alla funzione `import`) o al client mySql da finestra terminale (grazie al comando `source`).

Buon Lavoro!
