<?php include("top.html");?>

        <script src="../js/bacon.js" type="text/javascript"></script>
    </head>
    <body>

        <?php include("banner.html");?>

            <p>Welcome <span id="username"></span>!</p>
            <h1>The One Degree of Kevin Bacon</h1>
            <p>Type in an actor's name to see if he/she was ever in a movie with Kevin Bacon!</p>
            <p><img id="kevinphoto" alt="Kevin Bacon" ></p>

            <!-- ErrMsg: actor not in our database or actor with no films in our data base -->
            <div id="errMsg"></div>
            <!-- results: no error, results (all movies with Kevin Bacon) follow here -->
            <div id="results">
            </div>              

<?php include("bottom.html"); 
    
?>
