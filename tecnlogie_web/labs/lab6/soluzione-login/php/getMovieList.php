<?php 

# main program
if (!isset($_SERVER["REQUEST_METHOD"]) || $_SERVER["REQUEST_METHOD"] != "GET") {
    header("HTTP/1.1 400 Invalid Request");
    die("ERROR 400: Invalid request - This service accepts only GET requests.");
}

header("Content-type: application/json");
print "{\n";

include("common.php");
// controlla se è stato fatto login, nel caso continua a spedire in output le informazioni
// altrimenti 
if (isLogged()){
    print "  \"isLogged\": true, \n";
    printMovieListToJSON();
} else {
    print "  \"isLogged\": false, ";
    if(!$_SESSION["flash"]) {
        $_SESSION["flash"]="Please, login if you want to use this website.";    
    }
    print "  \"flash\": \"".$_SESSION["flash"]."\"";
}
    print "\n}\n";


function printMovieListToJSON() {
    global $db;
    global $firstN;
    global $lastN;

    // prendo le var passate con metodo GET
    $firstN = $_GET["firstname"];
    $lastN = $_GET["lastname"];
    if (isset($_GET["all"]) && $_GET["all"]=="true") 
        $all = true;
    else
        $all = false;
    
   
    $db = dbconnect();
    
    if ($all)
        $rows = all();
    else
        $rows = kevin();

    if ($rows == null) { 
        // se $rows è null, vuol dire che l'attore non è stato trovato nel db (id = null)
        $errMsg = "$firstN $lastN is not in our database";
        print "  \"errMsg\": \"".$errMsg."\"";
    } else {
        $count = $rows->rowCount();
        if ($count == 0) { // l'attore è stato trovato ma non ci sono film da mostrare
            if($all)
                $errMsg = "$firstN $lastN has no films in our database.";
            else
                $errMsg = "$firstN $lastN wasn't in any film with Kevin Bacon.";
        print "  \"errMsg\": \"".$errMsg."\"";
        } else {
            print "  \"firstname\": \"$firstN\", \n";
            print "  \"lastname\": \"$lastN\", \n";
            print "  \"movies\": ";
            // encode to json all the rows in our result set
            print json_encode($rows->fetchall());
        }
    }
} // end printMovieListToJSON
?>