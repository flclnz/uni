<?php
    if(!isset($_SESSION)) {session_start();}
    
    function isLogged() {
        if (!isset($_SESSION["name"])) {
            if (!isset($_SESSION["flash"])) {
                $_SESSION["flash"] = "Please, login if you want to use this website.";
            }
            return false;
        }
        else {
            return true;
        }
    }

    function dbconnect() {
        $db = new PDO('mysql:dbname=imdb;host=localhost:3306', "root", "");
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $db;
      }
    
    // verifica se l'utente e la password sono corretti
    function pwdVerify($username, $pwd) {
        # Nota del docente: username e password modificate in "root", "" 
        try {
            $db = dbconnect();
            $userN = $db->quote($username);
            $rows = $db->query("SELECT password FROM users WHERE name = $userN");
        } catch (PDOException $ex) {
            die('Database error: ' . $ex->getMessage());
        }
        if ($rows) { // controlla la corrispondenza utente / password
            foreach ($rows as $row) {
                $password = $row["password"];
                return $pwd === $password;
            }
        } else {
        return false; // user not found
        }
    }
    //questa funzione trova il miglior risultato e ne restituisce l'id. 
    function trovaID(){
        global $db, $firstN, $lastN;
        $likeN = $firstN."%";
        $likeQuoted = $db->quote($likeN);
        $lastNQuoted = $db->quote($lastN);
        try {
            $str = "SELECT MIN(id) id FROM actors
            WHERE first_name like $likeQuoted and last_name = $lastNQuoted 
            and film_count = (select MAX(film_count)
                            from actors
                            where first_name like $likeQuoted and last_name = $lastNQuoted)";
            $better = $db->query($str);    
 
        } catch (PDOException $ex){
            die('Database error: ' . $ex->getMessage());
        }
        //questa query restituisce i miglior risultato in caso di omomimia
        return $better; // null se l'attore non è nel db
    }
    
    /* questa funzione restituisce tutti i film in cui ha recitato l'attore 
       su cui è stata effettuata la ricerca. Si utilizza l'id del miglior 
       risultato (trovaID()) */
    function all(){
        global $db;
        $better = trovaID();
        $res = $better->fetch(PDO::FETCH_ASSOC);
        $tmp = $res["id"];
        $bt = strval($tmp);
        if ($bt != null) { 
            try{
                /* questa query restituisce tutti i film in cui ha recitato l'attore su cui
               è stata eseguita la ricerca */
                $rows = $db->query("SELECT m.name, m.year
                                FROM actors a JOIN roles r on a.id = r.actor_id JOIN movies m ON m.id = r.movie_id 
                                WHERE a.id = ".$db->quote($bt)."
                                order by m.year desc, m.name asc;");
            } catch(PDOException $ex) {
                die('Database error: ' . $ex->getMessage());
            }
            return $rows;
        } else { // attore non presente
            return $rows = null;
        }
    }
    /* questa funzione restituisce tutti i film in cui l'attore 
       su cui è stata effettuata la ricerca ha recitato con Kevin Bacon. 
       Si utilizza l'id del miglior risultato (trovaID()) */
    function kevin() {
        global $db;
        $better = trovaID();
        $res =$better->fetch(PDO::FETCH_ASSOC);
        $tmp = $res["id"];
        $bt = strval($tmp);
        if($bt != null) {
            $bacon = $db->quote("Bacon");
            $kevin = $db->quote("Kevin");
            try {
                /* questa query restituisce tutti i film in cui l'attore ricercato 
                ha recitato insieme a Kevin Bacon */
                $rows = $db->query("SELECT m.name, m.year
                FROM actors a JOIN roles r on a.id = r.actor_id JOIN movies m ON m.id = r.movie_id
                JOIN roles r2 on r2.movie_id = m.id JOIN actors a2 on a2.id = r2.actor_id
                WHERE a.id = ".$db->quote($bt)." and a2.first_name = ".$kevin. 
                        "and a2.last_name =".$bacon.
                        "order by m.year desc, m.name asc;");
            } catch(PDOException $ex) {
                die('Database error: ' . $ex->getMessage());
            }
            return $rows;
        } else { // attore non presente
            return $rows = null;
        }
    }
?>