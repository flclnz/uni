<?php 
   # main program
   if (!isset($_SERVER["REQUEST_METHOD"]) || $_SERVER["REQUEST_METHOD"] != "POST") {
       header("HTTP/1.1 400 Invalid Request");
       die("ERROR 400: Invalid request - This service accepts only POST requests.");
    }
    
    include("common.php");

    header("Content-type: application/json");
    print "{\n";

    // se è stata eseguita la procedura di login, verifica le credenziali
    if (isset($_POST["username"]) && isset($_POST["pwd"])) {
        $username = $_POST["username"];
        $pwd = $_POST["pwd"];
        if (pwdVerify($username, $pwd) == true) {
            if (isset($_SESSION)) {
                session_regenerate_id(TRUE);
            }
            $_SESSION["name"] = $username;
        } else {
            $_SESSION["flash"] = "invalid username or password";
        }
    }

    // return to caller true if logged in, false otherwise
    if(isLogged()){
        print " \"isLogged\": true, \n";
        print "  \"name\": \"".$_SESSION["name"]."\"";
    } else {
        print " \"isLogged\": false \n";
    }
    print "\n}";
?>