DROP TABLE IF EXISTS users;
CREATE TABLE users (
  id INT UNSIGNED NOT NULL PRIMARY KEY DEFAULT '0',
  name varchar(100) NOT NULL DEFAULT 'guest',
  password varchar(16) NOT NULL DEFAULT '0000'
);

INSERT INTO users VALUES 
(12, 'Kevin', '1234'), 
(31, 'Will', '5678'), 
(14, 'Tom', '0123'), 
(145, 'Steve', '4567');

