$(function() {
    
    $("#results").hide();
    $("#errMsg").hide();

    // get selected actor's movie list played with Kevin Bacon 
    // when corresponding submit button is pressed
    $("#searchkevin input[name='submit']").on("click",function() {
        $.get({
            url: "../php/getMovieList.php", 
            data: "firstname="+$("#searchkevin input[name='firstname']").val()
                +"&lastname="+$("#searchkevin input[name='lastname']").val(),
            datatype: "json",
            success: printMovieList
        });
        $("input[name='firstname']").val("");
        $("input[name='lastname']").val("");
    });
        
    // get selected actor's movie list when corresponding submit
    // button is pressed
    $("#searchall input[name='submit']").on("click",function() {
        $.get({
            url: "../php/getMovieList.php", 
            datatype: "json",
            data: "firstname="+$("#searchall input[name='firstname']").val()
                +"&lastname="+$("#searchall input[name='lastname']").val()
                +"&all=true",   // also all=true argument is passed to getMovieList.php
            success: printMovieList
        });
        $("input[name='firstname']").val("");
        $("input[name='lastname']").val("");
    });
});

/*  -----------------------
    Show results functions
    ----------------------- */

function printMovieList(json){
    // remove every row in the table containing previous results
    $(".rowMovie").remove();

    // remove kevin picture to list movies
    $("#kevinphoto").remove();
    
    // print error messages if the actor or actor's movies have not been found
    if(json.errMsg){
        $("#errMsg").text(json.errMsg);
        $("#errMsg").show();
        $("#results").hide();
    }
    else {
        $("#errMsg").text("");
        $("#errMsg").hide();
        $("#results").show();
        // insert actor's name information
        $("#firstN").text(json.firstname);
        $("#lastN").text(json.lastname); 
        var num =1;
        // print movies list
        json.movies.forEach(function(item){
            // create a new <tr> tag and add it to the table
            var tr = $('<tr></tr>');
            // add a class to every row that is added
            tr.addClass("rowMovie");
            // create three new <td> cells with movie's information
            var td1 = $('<td></td>');
            td1.addClass("ext");
            td1.text(num++);
            var td2 = $('<td></td>');
            td2.text(item.name);
            var td3 = $('<td></td>');
            td3.addClass("ext");
            td3.text(item.year);
            tr.append(td1,td2,td3);
            $("#list").append(tr);   
        });
    } 
    // scroll smoothly to the top
    $("html, body").animate({ scrollTop: 0 }, "slow");

}   // end printMovieList
