<!-- Progetto: Lab 04 (NerdLuv)
		Autore: Mantarro R.C. matricola 260632 -->
		
<?php include("top.html");

#CHECK FORM INPUT

$personality_type = array("ESTJ","ISTJ","ENTJ","INTJ","ESTP","ISTP","ENTP","INTP","ESFJ","ISFJ","ENFJ","INFJ","ESFP","ISFP","ENFP","INFP");

#Check personality type
if (!in_array($_REQUEST['perstype'], $personality_type)){
	?>
	<p><strong>Please insert a human personality.</strong></p>

	<?php
#check if age is a number
} else if (!ctype_digit($_REQUEST['age'])) {
	?>
	<p><strong>Please insert a human age.</strong></p>

	<?php
#check if fromage and toage are numbers
} else if (!ctype_digit($_REQUEST['fromage']) || !ctype_digit($_REQUEST['toage'])) {
	?>
	<p><strong>You can't seek an age that is not human.</strong></p>

	<?php
#check if max age is NOT lower than min age
} else if ($_REQUEST['fromage'] > $_REQUEST['toage']) {
	?>
	<p><strong>Min age must be lower than max age. Don't try to confuse us.</strong></p>

	<?php
} else {
	#Check if username is already in use
	$lines = file("singles.txt");
	$new_name = true;
	foreach ($lines as $line) {
		list($Name, $Gender, $Age, $Type, $Favos, $fromAge, $toage) = explode(",", trim($line));
		if ($Name == $_REQUEST['name']){
			$new_name = false;
			?>
			<p><strong>This username already exists, be more creative.</strong></p>
			<?php
		}
	}
	#if the username is new:
	if($new_name){ 
		#Write info new user in single.txt.
		$newUser = implode(",", $_REQUEST);
		file_put_contents("singles.txt", $newUser."\n", FILE_APPEND);

		?>

		<p>Welcome to NerdLuv, <?= $_REQUEST['name'] ?>!<br><br>Now <a href="matches.php">log in to see your matches!</a></p>
		<?php
	}
}
?>



<?php include("bottom.html"); ?>