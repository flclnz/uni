<!-- Progetto: Lab 04 (NerdLuv)
		Autore: Mantarro R.C. matricola 260632 -->
<?php include("top.html");
	$user = $_GET["name"];
	$userfilename = "singles.txt";
	#search for user in csv
	#if exists
	if (get_user($user, $userfilename) != NULL){
		list($userName, $userGender, $userAge, $userType, $userFavos, $userFromAge, $userToage, $userGenSeek) = get_user($user, $userfilename);
?>
		<div>
		<p><strong>Matches for  <?=$userName ?> </strong></p>
		</div>

		<?php
			$lines = file($userfilename);
			$comp = 0;
			foreach ($lines as $line) {
				list($Name, $Gender, $Age, $Type, $Favos, $fromAge, $toage) = explode(",", trim($line));
				#check compatibility
					//if both gender
				if ( $userGenSeek == "B") {
					$condition = ($userName != $Name) && ($userFavos == $Favos) && ((int)$userFromAge <= (int)$Age)
					&& ((int)$userToage >= (int)$Age) && isCompType($userType, $Type); }
					//else if male or female gender
				else {
					$condition = ($userName != $Name) && ($userGenSeek == $Gender) && ($userFavos == $Favos) && ((int)$userFromAge <= (int)$Age)
					&& ((int)$userToage >= (int)$Age) && isCompType($userType, $Type); }
				#if condition are satisfied
				if( $condition ) {
					$comp++;
        ?>
						<div class="match">
						<p><img src="http://www.cs.washington.edu/education/courses/cse190m/12sp/homework/4/user.jpg" alt="match user"><?=$Name ?></p>
						<ul>
							<li><strong>gender:</strong><?=$Gender ?></li>
							<li><strong>age:</strong><?=$Age ?></li>
							<li><strong>type:</strong><?=$Type ?></li>
							<li><strong>OS:</strong><?=$Favos ?></li>
						</ul>
						</div>
					<?php
		   		}
		   }
		   #if we found no compatibility
		   if ($comp == 0){
			         ?>
					<div>
					<p><strong>You're too geek we can't help ya</strong></p>
					</div>
				<?php
			}
	#else if user doesn't exist
	} else {
		        ?>
		<div>
		<p><strong>User not found!</strong></p>
		</div>	
		<?php	
	}
		?>





<?php include("bottom.html");

# Legge il file e cerca una linea con le info dell'utente.
# Restituisce riga file contenete info utente.
function get_user($uName, $filename) {
  $lines = file($filename);
  foreach ($lines as $line) {
    	if (strstr($line, ",", true) == $uName) {
      return explode(",", trim($line));
    }
  }
  return NULL;  # utente non trovato
}

# Compara due personalType, stringhe composte da 4 caratteri.
# Restituisce TRUE se trova almeno una lettera uguale stessa posizione.
function isCompType($type1, $type2) {
	for ($i=0; $i<4; $i++) {
		if ($type1[$i] == $type2[$i]) {
			return TRUE;
		}
	}
	return FALSE;
}

?>