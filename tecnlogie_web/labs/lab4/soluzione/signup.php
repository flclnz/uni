<?php include("top.html"); ?>
<!-- Progetto: Lab 04 (NerdLuv)
		Autore: Mantarro R.C. matricola 260632 -->

<div> <!-- form per creare un nuovo account  -->

<form action="signup-submit.php" method="POST">
	<fieldset>
	<legend>New User Signup:</legend>
	<div>
	<label><strong>Name:</strong></label><input type="text" name="name" size="16" autofocus><br>
	<label><strong>Gender:</strong></label>
		<label for="male">Male</label><input type="radio" name="gender" id="male" value="M">
		<label for="female">Female</label><input type="radio" name="gender" id="female" value="F" checked><br>
	<label><strong>Age:</strong></label><input type="text" name="age" size="6" maxlength="2"><br>
	<label><strong>Personality type:</strong></label><input type="text" name="perstype" size="6" maxlength="4">
	(<a href="http://www.humanmetrics.com/cgi-win/JTypes2.asp" target="_blank">Don't know your type?</a>)<br>
	<label><strong>Favorite OS:</strong></label>
	<select name="favoriteos">
		<option>Windows</option>
		<option>Mac OS X</option>
		<option selected="selected">Linux</option>
	</select><br>
	<label><strong>Seeking age:</strong></label>
	<input type="text" name="fromage" size="6" maxlength="2" placeholder="min"> to <input type="text" name="toage" size="6" maxlength="2" placeholder="max"><br>
		<label><strong>Favorite gender:</strong></label>
		<select name="favoriteGen">
			<option value="M">Male</option>
			<option value="F">Female</option>
			<option value="B">Both</option>
	</select><br>
	<input type="submit" value="Sign Up">
	</div>
	</fieldset>
</form>

</div>

<?php include("bottom.html"); ?>