// 15 real tiles
// 1 empy tile in the lower right corner
//
//     0   100  200  300               0   100  200  300    
//   0 +----+----+----+----+         0 +----+----+----+----+
//     | 0,0| 0,1| 0,2| 0,3|           | 0,0| 0,1| 0,2| 0,3|
// 100 +----+----+----+----+       100 +----+----+----+----+ 
//     | 1,0| 1,1| 1,2| 1,3|           | 1,0| 1,1| 1,2| 1,3|
// 200 +----+----+----+----+       200 +----+----+----+----+
//     | 2,0| 2,1| 2,2| 2,3|           | 2,0| 2,1| 2,2|empty
// 300 +----+----+----+----+       300 +----+----+----+----+
//     | 3,0| 3,1| 3,2|empty           | 3,0| 3,1| 3,2| 3,3|
//     +----+----+----+                +----+----+----+----+

// -----------------
// --- CONSTANTS ---
// -----------------

const TILE_DIM = 100; // dimension of a tile

const EMPTY_TILE_START_POS_X = 3; // starting x position of the empty tile
const EMPTY_TILE_START_POS_Y = 3; // starting y position of the empty tile
const EMPTY_TILE_START_ID = "#tile_3_3"; // starting id of the empty tile

const STARTING_COUNT_DOWN = 30000;   // set here the starting ms of your countdown

// ------------------------
// --- GLOBAL VARIABLES ---
// ------------------------

var emptyTile = {   // emptyTile while playing
    id: EMPTY_TILE_START_ID,
    x: EMPTY_TILE_START_POS_X ,
    y: EMPTY_TILE_START_POS_Y
};

var blockZindex = 1000; // to set an high z-index for putting on foreground a blocking div 

var score = 0;
var spanScore = $("<span></span>"); // add an element to show the total score

var intervalId, timeoutId; // to set a countdown

// --------------
// --- ONLOAD ---
// --------------

$(function() {
    // create the puzzle	
	createPuzzle();

    // the shufflebutton is hidden
    $("#shufflebutton").hide();
    // ... we will use a new alert window instead
    myAlert("Click below to shuffle<br>");
    
    // give the score display some style and show it floating on the right
    spanScore.css({
        "background-color": "grey",
        "margin": "auto",
        "width": "100px",
        "border": "3px solid green",
        "padding": "10px",
        "position": "absolute",
        "top": "150px",
        "right":"10%",
        "font-size": "14pt"
    });
    spanScore.html(`Score: ${score}`);
    spanScore.appendTo('body');
});

// -----------------------
// --- EVENT FUNCTIONS ---
// -----------------------

function shuffleClick() {
    // random number of moves (between 20 and 200)
    var	randomMoves = Math.floor((Math.random() * 200) + 20);

	for(var i = 0; i < randomMoves; i++) {
        // array of tiles that can be moved
        var movableTiles = [];
        
        // start from the empty tile, and look for at most four neighbors 
        // check the "movability" of each neighbor
        if (emptyTile.x!=0){
            movableTiles.push(`#tile_${emptyTile.x-1}_${emptyTile.y}`);
        }
        if (emptyTile.y!=0){
            movableTiles.push(`#tile_${emptyTile.x}_${emptyTile.y-1}`);
        }
        if (emptyTile.x!=3){
            movableTiles.push(`#tile_${emptyTile.x+1}_${emptyTile.y}`);
        }
        if (emptyTile.y!=3){
            movableTiles.push(`#tile_${emptyTile.x}_${emptyTile.y+1}`);
        }
        
        // move a tile at random among the ones that can be moved
        var rndTileId = movableTiles[Math.floor(Math.random() * movableTiles.length)];
		moveTile($(rndTileId));
    }
    
    var countdown = STARTING_COUNT_DOWN;  

    var spanCD = $("<span>-</span>"); // countdown is displayed in this span
    spanCD.html(`-${countdown/1000} secs`);

    // let the countdown appear with style
    spanCD.appendTo('body');
    spanCD.css({
        "background-color": "grey",
        "margin": "auto",
        "width": "100px",
        "border": "3px solid green",
        "padding": "10px",
        "position": "absolute",
        "top": "200px",
        "right":"10%",
        "font-size": "14pt"
    });


    // let the countdown be refreshed eavery sec.
    intervalId = setInterval(function(){
        countdown-=1000;
        spanCD.html(`-${countdown/1000} secs`);
        if (countdown == 3000){
            spanCD.css({"background-color": "darkred"});
        } else if (countdown == 2000){
            spanCD.css({"background-color": "crimson"});
        } else if (countdown == 1000){
            spanCD.css({"background-color": "red"});
        }
    },1000)
    // let the countdown begin
    timeoutId = setTimeout(function(){
        myAlert("GAME OVER! You failed to solve the puzzle!");
        spanCD.remove();
        clearInterval(intervalId);
    },countdown);

}

function tileClick() {
	// move the tile
    if(moveTile($(this))) {
        // if the puzzle is completed, show a message
        if(checkPuzzle()) {
            clearInterval(intervalId);
            clearTimeout(timeoutId);
            myAlert("CONGRATS! You solved the puzzle!");
        }   
    }
}

function tileMouseOver() {
    // get tile's indexes
    var {x, y} = getTileIndexes($(this));

    // highlight the tile if the tile can be moved
    if(checkCloseEmptyTile(x, y)) {	
		$(this).addClass("tile_ok");
	// de-highlight the tile if the tile is already highlighted
    } else if($(this).hasClass("tile_ok")) {
		$(this).removeClass("tile_ok");
    }
}

// -----------------
// --- FUNCTIONS ---
// -----------------

function createPuzzle() {
    var y = 0;
    var offset = 3;
    
    // get the div tiles
    var puzzleTiles = $('#puzzlearea div');
    
    puzzleTiles.on({
        click : tileClick,
        mouseover: tileMouseOver
    });

    puzzleTiles.addClass("tile");
    
    puzzleTiles.each(function(item) {
        // get the position in the grid
        x = item % 4;
        y = Math.floor(item / 4);

        // add all attributes
     

        $(this).css({"left" : `${TILE_DIM * x}px`});
        $(this).css({"top" : `${TILE_DIM * y}px`});

        $(this).css({"background-position" : `${-x * TILE_DIM}px ${y * -TILE_DIM}px`});
        
        $(this).attr("id",`tile_${x}_${y}`);
    });
}	

function moveTile(tile) {
   // get tile's indexes
   var {x, y} = getTileIndexes(tile);

    // if the tile can be moved
	if(checkCloseEmptyTile(x, y)) {
        // switch the position and the id with the empty tile with animation
        tile.animate({
            left: `${emptyTile.x*100}px`, 
            top: `${emptyTile.y*100}px`
        }, "fast");

        // unccomment to switch the position and the id with the empty tile with no animation 
        /* 
        tile.css({
            left: `${emptyTile.x*100}px`, 
            top: `${emptyTile.y*100}px`
        });*/
        
        var tileId = tile.attr("id");
        tile.attr("id",emptyTile.id);
	       
        emptyTile.x= x;
        emptyTile.y = y;
        emptyTile.id = tileId;
        
        return(true);
	}
    return(false);
}

function checkCloseEmptyTile(x, y) {
    	
    // if the current tile has close the empty tile, then return true
	if(Math.abs(emptyTile.y - y) == 1 && emptyTile.x - x == 0
      || Math.abs(emptyTile.x - x) == 1 && emptyTile.y- y == 0) {
        return(true);
    }
    // return false otherwise
    return(false);
}

function checkPuzzle() {
    // check if the empty tile is at the starting position
    if(emptyTile.id != EMPTY_TILE_START_ID) {
        return(false);
    }
    
    var puzzleTiles = $('.tile');

    var complete=true;

    // iterate all tiles
    puzzleTiles.each(function(item) {
		// get the position in the grid
        x = item % 4;
        y = Math.floor(item / 4);
        
        // return false if this tile is in the wrong position
        if($(this).attr("id")!=`tile_${x}_${y}`) {
            complete = false;
		    return(false);
        }
	});
    if (complete){
        score++;
        spanScore.html(`Score: ${score}`);
    }
    // return true if all tiles are in the correct position
    return(complete);
}	

function getTileIndexes(tile) {
       // get the tile's positions to return its grid's indexes
       var x = parseInt(tile.css("left"))/100;
       var y = parseInt(tile.css("top"))/100;

       // this function returns two values
       return {x,y};
}


function blockPage(){
    // create a block page: a div with high z-index, so the user cannot interact to 
    // any elements below it 
    blockdiv = $("<div id='blockpage'></div>");
    blockdiv.css (
        {
            "height": $(document).height(),
            "width" : $(window).width(),
            "position": "absolute",
            "top": "0",
            "left": "0",
            "background-color": "rgba(0,0,0,0.6)",
            "z-index": `${blockZindex}`
        }
    );
    blockdiv.appendTo("body");

    return blockdiv;
}

function  myAlert(msg){
    // this create a modal window that behaves like a pop-up, but it is not a pop-up

    // let's create a blocking layer. This will make modal the new window
    var blockdiv = blockPage();

    // create a new div
    div = $("<div></div>");

    // append it in the puzzlearea
    $("#puzzlearea").append(div);

    // give it some style and show it in the center of the puzzlearea
    div.css({
        "background-color": "grey",
        "opacity": '0.4',
        "margin": "auto",
        "width": "50%",
        "border": "3px solid green",
        "padding": "10px",
        "position": "relative",
        "top":"30%",
        "font-size": "14pt",
        "z-index": `${blockZindex+1}`,
    });

    // put some animation and special effect before showing the message
    div.animate({height: '50px', opacity: '0.6'}, "slow");
    div.animate({width: '300px', opacity: '0.8'}, "slow");
    div.animate({opacity: '1'}, "slow", function(){
        // at the end of the animation append a button and show the message
        btn = $("<button></button>");
        btn.html("New Game!");
        btn.on("click",function(){
            // when the button is pressed, the 'fake' alert window is removed
                $(this).parent().remove();
                blockdiv.remove();

                // reshuffle everything again!
                shuffleClick();
            })
        
        $(this).html(msg+"<br>");
        $(this).append(btn);
    });

   
}
