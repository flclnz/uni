var MAX_TIME = 5;      // 5 seconds for each question
var timer = null;      // holds ID of timer

$(function() {
  $("#guess").on("change", guessChange);
  nextProblem();
});

// Called when player's guess changes.  Checks whether guess is correct.
function guessChange(event) {
  var guess = $("#guess").val();
  var answer = $("#num1").html() * $("#num2").html();
  if (guess == answer) {
    increment("#correct");  // user got the right answer; give a point
  }
  increment("#total");       // move on to next problem
  nextProblem();
}

// Called by timer when time has elapsed (user ran out of time)
function tick() {
  var seconds = increment("#time", -1);
  if (seconds <= 0) {   // time up!
    increment("#total");
    nextProblem();
  }
}

// Chooses two new random numbers for the next quiz problem.
function nextProblem() {
  $("#num1").html(parseInt(Math.random() * 20) + 1);
  $("#num2").html(parseInt(Math.random() * 20) + 1);
  $("#guess").val("");
  $("#time").html(MAX_TIME);
  clearInterval(timer);
  timer = setInterval(tick, 1000);
}



// Adjusts the integer value of the text for the element with the
// given id by the given amount (by 1 if no amount is passed).
function increment(id,amount) {
  var number = parseInt($(id).html()) + (amount || 1);
  $(id).html(number);
  return number;
}



// copied from zip code example

function numbersOnly(event) {
  var ALLOWED = [Event.KEY_RETURN, Event.KEY_BACKSPACE, Event.KEY_LEFT, Event.KEY_RIGHT, Event.KEY_HOME, Event.KEY_END, Event.KEY_TAB];
  var zero = "0".charCodeAt(0);
  var nine = "9".charCodeAt(0);
  if ((event.keyCode < zero || event.keyCode > nine) &&
      ALLOWED.indexOf(event.keyCode) < 0) {
    event.stop();
  }
}
