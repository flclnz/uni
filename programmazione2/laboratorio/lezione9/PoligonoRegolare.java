import java.awt.*;
import javax.swing.*;

// Rappresenta un poligono eegolare a N lati.
public class PoligonoRegolare extends Figura {
	// Numero di lati del poligono 
    private int lati;
    // Raggio del cerchio in cui il poligono e' inscritto
    private int raggio;

    // Costruttore
    public PoligonoRegolare(int lati, int raggio) {
		this.lati = lati;
		this.raggio = raggio;
    }

    // Disegna il poligono con il contesto grafico @g, centrato nell'origine
    @Override
    public void draw(Graphics g) {
		final double angle = 2 * Math.PI / lati;

		// Il primo vertice si trovi sull'asse delle ascisse, 
		// alle coordinate (raggio, 0)
		int x = raggio;
		int y = 0;
		for (int i = 1; i <= lati; i++) {
		    // Calcola le coordinate del vertice successivo
		    int nx = (int) (raggio * Math.cos(angle * i));
		    int ny = (int) (raggio * Math.sin(angle * i));

		    // Disegna una linea dal vertice corrente (x,y) al
		    // successivo (nx,ny)
		    g.drawLine(x, y, nx, ny);

		    // Aggiorna il vertice corrente
		    x = nx;
		    y = ny;
		}
    }
}
