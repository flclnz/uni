// Cerchio.java
import java.awt.*;
import javax.swing.*;

public class Cerchio extends Figura {
	// Raggio del cerchio, in pixels
    private int raggio;

    // Costruttore
    public Cerchio(int raggio) {
        this.raggio = raggio;
    }

    // Disegna il cerchio con il contesto grafico @g, centrato nell'origine
    @Override
    public void draw(Graphics g) {
		// Il metodo drawOval richiede le coordinate dell'angolo superiore 
		// sinistro del rettangolo che racchiude l'ovale da disegnare. 
		// Per questo  motivo si usano le coordinate (-raggio,-raggio).
		// I successivi 2 parametri di drawOval sono la larghezza e
		// l'altezza dell'ovale. Nel nostro caso, usiamo per entrambi
		// il doppio del raggio
		g.drawOval(-raggio, -raggio, 2 * raggio, 2 * raggio);
    }
}
