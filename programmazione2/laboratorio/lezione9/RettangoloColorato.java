// RettangoloColorato.java
import java.awt.*;
import javax.swing.*;

// Specializzazione della classe Rettangolo, che aggiunge un attributo Color
// al disegno del rettangolo.
public class RettangoloColorato extends Rettangolo {
	// Colore da utilizzare per il disegno del rettangolo
    private Color colore;

    // Costruttore
    public RettangoloColorato(int base, int altezza, Color colore) {
		// richiamo il costruttore della classe base
		super(base, altezza);
		// campo colore specifico della classe RettangoloColorato
		this.colore = colore;
    }

    // Disegna il rettangolo colorato con il contesto grafico @g.
    @Override
    public void draw(Graphics g) {
		// Acquisisci il colore attualmente impostanto in @g, per poterlo 
		// ripristinare alla fine delle operazioni di disegno di questo metodo.
		Color prevColor = g.getColor();

		// Cambia il colore usato per le successive operazioni grafiche
		g.setColor(colore);

		// Delega alla classe base le operazioni di disegno del rettangolo
		super.draw(g);

		// ripristina il colore precedente
		g.setColor(prevColor);
    }
}
