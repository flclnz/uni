// Metodi statici per la manipolazione di liste generiche
public class NodeUtil {

    // Ritorna il numero di elementi nella lista @p. Versione iterativa.
    public static <T> int size(Node<T> p){
        int n = 0;
        while (p != null) {
            n++;
            p = p.getNext();
        }
        return n;
    }

    // Ritorna il numero di elementi nella lista @p. Versione ricorsiva.
    public static <T> int sizeRec(Node<T> p){
        if (p == null)
            return 0;
        return 1 + sizeRec(p.getNext());
    }

    // Ritorna il numero di occorrenze di @x in @p. Versione iterativa.
    public static <T> int  occurrences(Node<T> p, T x){
        int n = 0;
        while (p != null) {
            if (p.getElem().equals(x)) 
                n++;
            p = p.getNext();
        }
        return n;
    }

    // Ritorna il numero di occorrenze di @x in @p. Versione ricorsiva.
    public static <T> int occurrencesRec(Node<T> p, T x){
        if (p == null) 
            return 0;
        else if (p.getElem().equals(x)) 
            return 1 + occurrencesRec(p.getNext(), x);
        else 
            return occurrencesRec(p.getNext(), x);
    }

    // Ritorna true se tutti gli elementi nella lista @p compaiono 
    // nello stesso ordine anche nella lista @q, e false altrimenti. 
    // Versione iterativa.
    public static <T> boolean included(Node<T> p, Node<T> q){
        while (p != null && q != null) {
            if (p.getElem().equals(q.getElem())) 
                p = p.getNext();
            q = q.getNext();
        }
        return p == null;
    }

    // Ritorna true se tutti gli elementi nella lista @p compaiono 
    // nello stesso ordine anche nella lista @q, e false altrimenti. 
    // Versione ricorsiva.
    public static <T> boolean includedRec(Node<T> p, Node<T> q){
        return p == null ||
                (q != null && ((p.getElem().equals(q.getElem())
                               && includedRec(p.getNext(), q.getNext()))
                 || includedRec(p, q.getNext())));
    }

    // Ritorna una nuova lista che contiene gli stessi elementi di @p, 
    // in ordine inverso. Versione iterativa.
    public static <T> Node<T> reverse(Node<T> p){
        Node<T> q = null;
        while (p != null) {
            q = new Node<>(p.getElem(), q);
            p = p.getNext();
        }
        return q;
    }

    // Metodo di supporto per l'inversione ricorsiva di liste.
    private static <T> Node<T> reverseAux(Node<T> p, Node<T> q){
        return p == null ? q : reverseAux(p.getNext(),
                                          new Node<>(p.getElem(), q));
    }

    // Ritorna una nuova lista che contiene gli stessi elementi di @p, 
    // in ordine inverso. Versione ricorsiva.
    public static <T> Node<T> reverseRec(Node<T> p){ 
        return reverseAux(p, null); 
    }

    // Data una lista di liste @p, ritorna una nuova lista di interi in
    // cui ciascun elemento è la lunghezza della i-esima lista in @p.
    // Versione iterativa.
    public static <T> Node<Integer> listOfListsSizes(Node<Node<T>> p){
        Node<Integer> q = null;
        while (p != null) {
            Node<T> currentNestedList = p.getElem();
            q = new Node<>(NodeUtil.size(currentNestedList), q);
            p = p.getNext();
        }
        return reverse(q);
    }

    // Data una lista di liste @p, ritorna una nuova lista di interi in
    // cui ciascun elemento è la lunghezza della i-esima lista in @p.
    // Versione ricorsiva.
    public static <T> Node<Integer> listOfListsSizesRec(Node<Node<T>> p){
        if (p == null)
            return null;
        return new Node<>(NodeUtil.size(p.getElem()),
                          listOfListsSizesRec(p.getNext()));
    }

    // Stampa in output gli elementi della lista @p.
    public static <T> void printList(Node<T> p){
        System.out.print("[ ");
        while (p != null){
            System.out.print(p.getElem()+" ");
            p = p.getNext();
        }
        System.out.print("]\n");
    }
}
