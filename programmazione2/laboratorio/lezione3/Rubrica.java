public class Rubrica
{/*INVARIANTE: (1) un nome non compare in due contatti 
 * (2) 0<=numContatti <= lunghezza vettore contatti */
  
  private int numContatti;
  private Contatto[] contatti;

  public Rubrica(int maxContatti)
  {numContatti = 0;
    contatti = new Contatto[maxContatti];
   }
  
  public int getNumContatti(){return numContatti;}
  
  public void scriviOutput()
  {int i=0;
    while(i < numContatti){contatti[i].scriviOutput();i++;}
  }
  
  private int cercaIndice(String n)
//restituisce numContatti se n non c'e'
  {int i=0;
    while (i < numContatti)
    {if (contatti[i].getNome().equalsIgnoreCase(n)) return i; i++;}
    return numContatti;
  }
  
  public boolean presente(String n)// con "" che rappresenta fallimento
   {return (cercaIndice(n) < numContatti);}
  
  public String cercaEmail(String n)
   {int i = cercaIndice(n);
     if (i<numContatti) return contatti[i].getEmail();
     else return "";
  }
  
  public boolean piena()
    {return (numContatti == contatti.length);}
  
  public boolean aggiungi(String n, String e)
   {if (presente(n)) return false;
     if (piena())        return false;
     contatti[numContatti]= new Contatto(n,e);
     numContatti++;
     return true;
  }
  
  public boolean rimuovi(String n)
   {int i=cercaIndice(n);
     if (i==numContatti) return false;
     --numContatti;//numContatti=numContatti-1
     contatti[i] = contatti[numContatti];
     return true;
  }
  
  public boolean cambiaNome(String n, String n2)
   {int i=cercaIndice(n),  j=cercaIndice(n2);
     if ( (i==numContatti) || (j<numContatti) ) return false;
     contatti[i].setNome(n2);
     return true;
  }
  
  public boolean cambiaEmail(String n, String e2)
   {int i=cercaIndice(n);
    if (i==numContatti) return false;
    contatti[i].setEmail(e2);
    return true;
  }
  
}