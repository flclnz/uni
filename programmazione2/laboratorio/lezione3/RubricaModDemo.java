public class RubricaModDemo {
public static void main(String[] args) {
// inizialmente, la rubrica ha capacita' 0 (caso limite)
// inserendo 6 elementi, testiamo anche l'incremento della capacita'
RubricaMod R = new RubricaMod(0);
System.out.println("(1) Rubrica con contatti a,b,c,d,e,f: ");
// aggiungo in ordine non lessicografico crescente
System.out.println("Inserisco <c,c@ditta>");
R.aggiungi("c","c@ditta");
System.out.println(R);
System.out.println("Inserisco <b,b@ditta>");
R.aggiungi("b","b@ditta");
System.out.println(R);
System.out.println("Inserisco <a,a@ditta>");
R.aggiungi("a","a@ditta");
System.out.println(R);
System.out.println("Inserisco <e,e@ditta>");
R.aggiungi("e","e@ditta");
System.out.println(R);
System.out.println("Inserisco <f,f@ditta>");
R.aggiungi("f","f@ditta");
System.out.println(R);
System.out.println("Inserisco <d,d@ditta>");
R.aggiungi("d","d@ditta");
System.out.println(R);
System.out.println("(2) Rimuovo a"); // rimuovo il primo elemento
R.rimuovi("a");
System.out.println(R);
System.out.println("(3) Rimuovo f"); // rimuovo l'ultimo elemento
R.rimuovi("f");
System.out.println(R);
System.out.println("(4) Rimuovo d"); // rimuovo un elemento centrale
R.rimuovi("d");
System.out.println(R);
System.out.println("(5) Aggiungo b (ma c'e' gia'): successo = " +
R.aggiungi("b","e"));
System.out.println(R);
System.out.println("(6) Modifico b in b2: successo = " +
R.cambiaNome("b","b2"));
System.out.println(R);
System.out.println("(7) Modifico e in b2 (ma b2 c'e' gia'): successo = " +
R.cambiaNome("e","b2"));
System.out.println(R);

System.out.println("(8) Modifico e in a2: successo = " +
R.cambiaNome("e","a2"));
System.out.println(R);
System.out.println("(9) Inserisco <g,g@ditta>");
R.aggiungi("g","g@ditta");
System.out.println(R);
System.out.println("(10) Modifico c in a3: successo = " +
R.cambiaNome("c","a3"));
System.out.println(R);
System.out.println("(11) Modifico b@ditta in b2@ditta: successo = " +
R.cambiaEmail("b2","b2@ditta"));
System.out.println(R);
}
}