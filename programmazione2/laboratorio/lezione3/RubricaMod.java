public class RubricaMod {// Invariante: 
// 1.una rubrica non contiene lo stesso nome due volte
// 2. (0<=numContatti <= lunghezza vettore contatti)
// 3. i contatti sono memorizzati tra 0 e numContatti-1
// 4. i contatti sono in ordine alfabetico
private int numContatti; //all'inizio vale 0
// array pre-allocato di contatti
private Contatto[] contatti; //all'inizio vale null
// Crea una nuova rubrica con @maxContatti contatti iniziali vuoti.
public RubricaMod(int maxContatti) {
//costruisce una rubrica con massimo numero di contatti = maxContatti
contatti = new Contatto[maxContatti];
//all'inizio i contatti significativi nella rubrica sono 0
numContatti = 0;
//all'inizio tutti i contatti nella rubrica non sono significativi:
//hanno nome e email uguali a null
}
// La nuova rubrica costruita soddisfa l'invariante
// Ritorna il numero di contatti attualmente inseriti nella rubrica
public int getNumContatti() {
return numContatti;
}
/** NOTA: non forniamo un metodo get per ottenere il vettore dei contatti:
conoscendolo, un'altra classe potrebbe leggere e modificare i
contatti in modo errato (in contraddizione con l'invariante) */
// Stampa su terminale una rappresentazione testuale della rubrica
public String toString() {int i = 0;
String s="Num. contatti = " + numContatti;
//Inseriamo i contatti di indice da 0 fino a numContatti-1.
//Gli altri contatti sono privi di significato
 while (i < numContatti) {s = s + "\n" + contatti[i].toString(); ++i;}
 return s;}
// Il metodo cercaIndice(n) restituisce l'unico indice i di un contatto
// di nome @n, se tale indice esiste; restituisce numContatti se tale indice
// non esiste. Il metodo cercaIndice(n) e' privato sempre per evitare che
// le altre classi modifichino un contatto in modo errato.
// Il metodo sfrutta il fatto che l'array che ospita i contatti e'

// lessicograficamente ordinato in senso crescente rispetto ai nomi dei
// contatti ed esegue una ricerca binaria
private int cercaIndice(String n) {
return cercaIndiceAux(n, 0, numContatti-1);
}
// Metodo wrapper ricorsivo che effettua la ricerca dicotomica del contatto
// di nome @n nella rubrica.
// Restituisce l'unico indice i di un contatto di nome @n
// presente fra le posizioni @start e @end **INCLUSE**, se tale indice esiste.
// Se non viene trovato, restituisce numContatti.
private int cercaIndiceAux(String n, int start, int end) {
// caso base 1: contatto non trovato
if(start > end)
return numContatti;
int mid = (start + end) / 2;
int comp = n.compareToIgnoreCase(contatti[mid].getNome());
if (comp == 0)
return mid; // caso base 2: contatto trovato, ritornane l'indice
// passo induttivo: discesa ricorsiva
if (comp < 0)
return cercaIndiceAux(n, start, mid-1);
else
return cercaIndiceAux(n, mid+1, end);
}
// Ritorna true se nella rubrica esiste un contatto di nome @n
public boolean presente(String n) {
return (cercaIndice(n) < numContatti);
}
// Ritorna true se la rubrica e' piena, cioe' se il numContatti e'
// uguale al numero di elementi che possiamo inserire nel vettore contatti
public boolean piena() {
return (numContatti == contatti.length);
}
// Aggiunge un nuovo contatto alla rubrica, con nome @n ed email @e
// Ritorna true in caso di successo, false se il contatto gia' esiste
public boolean aggiungi(String n, String e) {
if (presente(n))
return false;//la rubrica contiene gia' n: fallimento
if (piena())
estendi();//rubrica e' piena: raddoppio della capacita'
// aggiungo il nuovo contatto nella posizione lessicograficamente corretta
// (cioe' nella posizione correntemente occupata dall'elemento immediatamente
// successivo a quello da inserire, se tale elemento esiste; nella posizione
// numContatti, altrimenti)
int i = cercaIndiceInCuiInserire(n);
int j = numContatti-1;
while (j >= i) {
contatti[j+1] = contatti[j];
--j;
}
contatti[i] = new Contatto(n,e);
++numContatti;//aggiorno il numero degli elementi
return true;//successo
}
// Rimuove dalla rubrica il contatto di nome @n, se esiste
// Ritorna true se il contatto e' stato rimosso, false se non esiste
public boolean rimuovi(String n) {
int i = cercaIndice(n);
if (i == numContatti)
return false; // il contatto manca: fallimento
// se invece il contatto c'e': diminuiamo di uno i contatti
--numContatti;
// spostiamo tutti i contatti indietro di uno sovrascrivendo il contatto numero i
while (i < numContatti) {
contatti[i] = contatti[i+1];
++i;
}
return true; // successo
}
// Cerco un contatto di nome @n e cambio il suo nome a @n2, se esiste
// Ritorna true se il contatto e' stato modificato, false se non esiste
public boolean cambiaNome(String n, String n2) {
if (presente(n2))
return false; // la rubrica contiene gia' n2: fallimento
int i = cercaIndice(n);
if (i == numContatti)
return false; // contatto di nome n non trovato:fallimento
//se troviamo il contatto, ne recuperiamo l'email, lo cancelliamo e poi
//lo reinseriamo con il nome modificato rispettando l'ordine lessicografico
String email = contatti[i].getEmail();
//per ragioni di efficienza, non invochiamo rimuovi(), in quanto abbiamo
//gia' l'indice dell'elemento da rimuovere
--numContatti;
while (i < numContatti) {
contatti[i] = contatti[i+1];
++i;
}
aggiungi(n2,email);
return true;
}
// Cerco un contatto di nome @n e cambio la sua email a @e2, se esiste
// Ritorna true se il contatto e' stato modificato, false se non esiste
public boolean cambiaEmail(String n, String e2) {
int i = cercaIndice(n);
if (i == numContatti)
return false; // contatto di nome n non trovato:fallimento
//se troviamo il contatto ne modifichiamo la email
contatti[i].setEmail(e2);
return true;
}
// Raddoppia le dimensioni dell'array che ospita i contatti.
// Se l'array ha dimensione 0 (zero), lo sostituisce con un array di dimensione 1.
private void estendi() {
if(contatti.length == 0)
contatti = new Contatto[1];
else {
Contatto[] newContatti = new Contatto[2 * contatti.length];
for(int i = 0; i < numContatti; i++) {
newContatti[i] = contatti[i];
}
contatti = newContatti;
}
assert 0 <= numContatti && numContatti < contatti.length;
}
// Restituisce l'indice dell'array che ospita i contatti in cui
// un contatto con nome n deve essere inserito, in base all'ordine
// lessicografico. Considera la porzione di array da start a end-1
// Si potrebbe accelerare usando la ricerca dicotomica
private int cercaIndiceInCuiInserire(String n){
int i = 0;
boolean trovato = false;
while (i<numContatti && !trovato) {
if (n.compareToIgnoreCase(contatti[i].getNome()) < 0)
trovato = true;
else
++i;
}
return i;
}
}

