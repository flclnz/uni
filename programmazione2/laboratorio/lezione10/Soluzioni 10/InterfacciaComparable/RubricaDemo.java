public class RubricaDemo {
    public static void main(String[] args) {
        Rubrica R = new Rubrica(100);
        System.out.println("(1) Rubrica con contatti c,b,a: ");
        R.aggiungi("c","c@ditta");
        R.aggiungi("b","b@ditta");
        R.aggiungi("a","a@ditta");
        R.scriviOutput();

        System.out.println("(2) Rubrica con contatti c,b,a dopo ordinamento: ");
        R.sort();
        R.scriviOutput();    

        System.out.println("(3) Presente b (ordinato): " + R.presente("b"));

        System.out.println("(4) Rimuovo a");
        R.rimuovi("a");
        R.scriviOutput();

        System.out.println("(5) Aggiungo b (ma c'e' gia'): successo = " + 
                           R.aggiungi("b","e"));
        R.scriviOutput();
         
        System.out.println("(6) Modifico b in c2: successo = " + 
                           R.cambiaNome("b","c2"));
        R.scriviOutput();

        System.out.println("(7) Presente c (NON ordinato): " + R.presente("c"));

        System.out.println("(8) Modifico b@ditta in b2@ditta: successo = " + 
                           R.cambiaEmail("c2","b2@ditta"));
        R.scriviOutput();
    }  
}
