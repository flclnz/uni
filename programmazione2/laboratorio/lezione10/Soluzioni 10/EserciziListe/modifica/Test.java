public class Test {
 public static void main(String[] args) {
        MyList l1 = new MyList();
        l1.insert(5);
        System.out.println("prima: " + l1);
        l1.modifica();
        System.out.println("dopo:  " + l1);

        MyList l2 = new MyList();
        l2.insert(1);
        l2.insert(3);
        l2.insert(5);
        System.out.println("prima: " + l2);
        l2.modifica();
        System.out.println("dopo:  " + l2);

        MyList l3 = new MyList();
        l3.insert(4);
        l3.insert(0);
        l3.insert(1);
        l3.insert(-1);
        System.out.println("prima: " + l3);
        l3.modifica();
        System.out.println("dopo:  " + l3);
    }
}
