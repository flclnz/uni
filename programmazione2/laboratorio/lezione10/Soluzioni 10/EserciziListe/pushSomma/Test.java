public class Test {
 public static void main(String[] args) {
  MyList l1 = new MyList();
        System.out.println("prima: " + l1);
        l1.pushSomma();
        System.out.println("dopo:  " + l1);

  MyList l2 = new MyList();
  l2.insert(1);
        System.out.println("prima: " + l2);
        l2.pushSomma();
        System.out.println("dopo:  " + l2);

  MyList l3 = new MyList();
  l3.insert(-2);
  l3.insert(1);
        System.out.println("prima: " + l3);
        l3.pushSomma();
        System.out.println("dopo:  " + l3);

  MyList l4 = new MyList();
  l4.insert(-4);
  l4.insert(-6);
  l4.insert(7);
  l4.insert(4);
  l4.insert(-2);
  l4.insert(2);
        System.out.println("prima: " + l4);
        l4.pushSomma();
        System.out.println("dopo:  " + l4);
 }
}
