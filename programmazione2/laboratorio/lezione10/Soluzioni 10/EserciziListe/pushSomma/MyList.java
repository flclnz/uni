// MyList.java
public class MyList {
    private Node first; // Riferimento al primo nodo della lista

    public MyList() {
        this.first = null;
    }

    public void insert(int elem) {
        first = new Node(elem, first);
    }

    @Override
    public String toString() {
        String res = "";
        for (Node p = first; p != null; p = p.getNext()) {
            res += p.getElem();
            if (p.getNext() != null) res += ", ";
        }
        return res;
    }

    public void pushSomma() {
        int somma = 0;
        Node p = first;
        while (p != null) {
            if (p.getElem() > 0) 
                somma += p.getElem();
            p = p.getNext();
        }
        this.insert(somma);
    }
}
