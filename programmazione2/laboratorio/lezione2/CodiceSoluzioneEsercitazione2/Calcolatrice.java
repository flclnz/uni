// calcolatrice in notazione RPN implementata utilizzando la classe Stack
public class Calcolatrice {

    public static void main(String[] args) {
        // leggi l'espressione da valutare dalla riga di comando
        assert args.length == 1;
        String code = args[0];

        // crea la pila di valutazione degli argomenti usando la classe Stack
        int pc = 0;
        Stack s = new Stack(100);

        // Valuta l'espressione scritta in notazione RPN
        while (pc < code.length()) {
            char c = code.charAt(pc);
            if (c >= '0' && c <= '9') {
                s.push(c - '0');
            } 
            else if (c == '+') {
                int a = s.pop();
                int b = s.pop();
                s.push(a + b);
            } 
            else if (c == '*') {
                int a = s.pop();
                int b = s.pop();
                s.push(a * b);
            }
            pc++;
        }
        int ris = s.pop();
        
        if (!s.empty())
            System.out.println("errore nell'espressione");
        else
            System.out.println(ris);
    }
}
