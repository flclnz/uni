// Matrice rettangolare di interi
public class Matrix {

    // array bidimensionale m*n di interi che rappresenta la matrice
    private int[][] data;

    public Matrix(int rows, int columns) {
        assert rows > 0 && columns > 0;
        data = new int[rows][columns];
    }

    // ritorna il valore dela matrice in posizione (r,c)
    public int get(int r, int c) {
        assert 0 <= r && r < rows();
        assert 0 <= c && c < columns();
        return data[r][c];
    }

    // imposta il valore in posizione (r,c) ad x
    public void set(int r, int c, int x) {
        assert 0 <= r && r < rows();
        assert 0 <= c && c < columns();
        data[r][c] = x;
    }

    // ritorna il numero di righe della matrice
    public int rows() {
        return data.length;
    }

    // ritorna il numero di colonne della matrice
    public int columns() {
        return data[0].length;
    }

    // somma di matrici di uguali dimensioni
    // la somma viene ritornata come una nuova matrice
    public Matrix add(Matrix b) {
        assert rows() == b.rows();
        assert columns() == b.columns();
        Matrix c = new Matrix(rows(), columns());
        for (int i = 0; i < rows(); i++)
            for (int j = 0; j < columns(); j++)
                c.set(i, j, get(i, j) + b.get(i, j));
        return c;
    }

    // ritorna il prodotto di matrici.
    // La matrice ritornata ha dimensioni [this->rows(), b->columns()]
    public Matrix mul(Matrix b) {
        assert columns() == b.rows();
        Matrix a = this;
        Matrix c = new Matrix(a.rows(), b.columns());
        for (int i = 0; i < a.rows(); i++)
            for (int j = 0; j < b.columns(); j++) {
                int s = 0;
                for (int k = 0; k < a.columns(); k++)
                    s += a.get(i, k) * b.get(k, j);
                c.set(i, j, s);
            }
        return c;
    }

    // elevamento a potenza intera di matrici
    public Matrix pow(int n) {
        assert rows() == columns();
        if (n == 0) return identity(rows());
        else return pow(n - 1).mul(this);
    }

    // matrice identita' quadrata di dimensione @side
    public static Matrix identity(int side) {
        assert side > 0;
        Matrix m = new Matrix(side, side);
        for (int i = 0; i < side; i++)
            for (int j = 0; j < side; j++)
                m.set(i, j, i == j ? 1 : 0);
        return m;
    }
}
