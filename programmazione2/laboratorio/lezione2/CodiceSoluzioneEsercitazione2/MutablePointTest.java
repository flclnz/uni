public class MutablePointTest {
    public static void main(String[] args) {
        MutablePoint pt1 = new MutablePoint(2, 4);
        MutablePoint pt2 = new MutablePoint(6, 11);

        System.out.println("2, 4  ==  " + pt1.getX() + ", " + pt1.getY());

        pt1.translate(1, 3);
        System.out.println("3, 7  ==  " + pt1.getX() + ", " + pt1.getY());

        // distanza (3,7) e (6,11) e' pari a 5 (triangolo 3,4,5)
        System.out.println("5     ==  " + pt1.distance(pt2));

        // ruota pt1 di 90 gradi in senso antiorario
        pt1.rotate(Math.PI/2.0);
        System.out.println("-7, 3 ==  " + pt1.getX() + ", " + pt1.getY());
    }
}