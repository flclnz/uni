public class Mistero {
    
    public static int mistero(int n) {
        assert n >= 0;
        Matrix m = new Matrix(2, 2);
        m.set(0, 0, 1);
        m.set(0, 1, 1);
        m.set(1, 0, 1);
        m.set(1, 1, 0);
        return m.pow(n).get(0, 0);
    }

    public static void main(String[] args) {
        assert args.length == 1;
        System.out.println(mistero(Integer.parseInt(args[0])));
    }
}
