public class TestString {

    // ritorna la stringa piu' lunga in un array di stringhe
    public static String longest(String[] s) {
        assert s != null && s.length > 0;
        int l = 0;
        for (int i = 1; i < s.length; i++)
            if (s[i].length() > s[l].length())
                l = i;
        return s[l];
    }

    // ritorna la concatenazione di tutte le stringhe nell'array @s
    public static String concatAll(String[] s) {
        assert s != null;
        String result = "";
        for (int i = 0; i < s.length; i++)
            result = result.concat(s[i]);
        return result;
    }

    // elimina gli spazi iniziali e finali della stringa s
    public static String trim(String s) {
        int i = 0; // indice del primo carattere diverso da uno spazio
        while (i < s.length() && s.charAt(i) == ' ') 
            i++; 
        int j = s.length(); // j-1 = indice dell'ultimo carattere != ' '
        while (j > i && s.charAt(j - 1) == ' ') 
            j--;
        // taglia la stringa nell'intervallo semi-aperto [i, j)
        return s.substring(i, j);
    }

    // testa i tre metodi utilizzando gli argomenti passati a java sulla riga di comando
    public static void main(String[] args) {
        System.out.println("|" + longest(args) + "|");
        System.out.println("|" + concatAll(args) + "|");
        assert args.length > 0;
        System.out.println("|" + trim(args[0]) + "|");
    }
}
