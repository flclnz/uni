public class MatrixTest {

	public static boolean comparaMatriceArray(Matrix m, int[][] sol) {
		if (m.rows() != sol.length || m.columns() != sol[0].length) {
			System.out.println("NO dimensioni diverse.");
			return false; // diverse
		}

		for (int i=0; i<sol.length; i++) {
			assert sol[i].length == m.columns();
			for (int j=0; j<sol[i].length; j++) {
				if (m.get(i, j) != sol[i][j]) {
					System.out.println("NO elementi diversi.");
					return false; // diverse
				}
			}
		}

		System.out.println("OK.");
		return true;
	}
	
	public static void main(String[] args) {
    	// crea una matrice con numeri naturali progressivi
    	Matrix m1 = new Matrix(2, 3);
    	for (int i=0; i<m1.rows(); i++)
    		for (int j=0; j<m1.columns(); j++)
    			m1.set(i, j, i*m1.columns() + j);
    	int[][] sol1 = {{0,1,2}, {3,4,5}};
    	comparaMatriceArray(m1, sol1);

    	// crea una matrice di soli uno
    	Matrix m2 = new Matrix(2, 3);
    	for (int i=0; i<m2.rows(); i++)
    		for (int j=0; j<m2.columns(); j++)
    			m2.set(i, j, 1);
    	int[][] sol2 = {{1,1,1}, {1,1,1}};
    	comparaMatriceArray(m2, sol2);

    	// somma m1 ed m2
    	Matrix m12 = m1.add(m2);
    	int[][] sol12 = {{1,2,3}, {4,5,6}};
    	comparaMatriceArray(m12, sol12);

    	// crea una matrice 3*2 con numeri naturali progressivi
    	Matrix m3 = new Matrix(3, 2);
    	for (int i=0; i<m3.rows(); i++)
    		for (int j=0; j<m3.columns(); j++)
    			m3.set(i, j, i*m3.columns() + j);
    	int[][] sol3 = {{0,1}, {2,3}, {4,5}};
    	comparaMatriceArray(m3, sol3);

    	// calcola il prodotto m1*m3
    	Matrix m1x3 = m1.mul(m3);
    	int[][] sol1x3 = {{10, 13}, {28, 40}};
    	comparaMatriceArray(m1x3, sol1x3);

    	// calcola potenze di m1x3
    	Matrix pow2 = m1x3.pow(2);
    	int[][] sol4 = {{464, 650}, {1400, 1964}};
    	comparaMatriceArray(pow2, sol4);

    	// imposta i valori di pow2 usando i metodi set
    	pow2.set(0, 0, 4);
        pow2.set(0, 1, 2);
        pow2.set(1, 0, 3);
        pow2.set(1, 1, 5);
        Matrix pow2_4 = pow2.pow(4);
        int[][] sol2_4 = {{970, 954}, {1431, 1447}};
    	comparaMatriceArray(pow2_4, sol2_4);

    	// verifica che la potenza 0 sia l'identita'
    	Matrix pow0 = m1x3.pow(0);
    	int[][] sol5 = {{1, 0}, {0, 1}} ;
    	comparaMatriceArray(pow0, sol5);
	}
}

