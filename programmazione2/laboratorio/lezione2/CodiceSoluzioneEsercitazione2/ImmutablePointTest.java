public class ImmutablePointTest {
    public static void main(String[] args) {
        ImmutablePoint pt1 = new ImmutablePoint(2, 4);
        ImmutablePoint pt2 = new ImmutablePoint(6, 11);

        System.out.println("2, 4  ==  " + pt1.getX() + ", " + pt1.getY());

        ImmutablePoint pt3 = pt1.translate(1, 3);
        System.out.println("3, 7  ==  " + pt3.getX() + ", " + pt3.getY());

        // distanza (3,7) e (6,11) e' pari a 5 (triangolo 3,4,5)
        System.out.println("5     ==  " + pt3.distance(pt2));

        // ruota pt3 di 90 gradi in senso antiorario
        ImmutablePoint pt4 = pt3.rotate(Math.PI/2.0);
        System.out.println("-7, 3 ==  " + pt4.getX() + ", " + pt4.getY());
    }
}