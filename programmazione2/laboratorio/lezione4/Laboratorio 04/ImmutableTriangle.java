// classe immutabile che rappresenta un triangolo su un piano cartesiano
public class ImmutableTriangle {
    // i tre punti (immutabili) che formano i vertici del triangolo
    private ImmutablePoint a;
    private ImmutablePoint b;
    private ImmutablePoint c;

    // costruttore
    public ImmutableTriangle(ImmutablePoint a, ImmutablePoint b, ImmutablePoint c) {
        assert (a != null) && (b != null) && (c != null);
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public ImmutablePoint getA() {
        return a;
    }

    public ImmutablePoint getB() {
        return b;
    }

    public ImmutablePoint getC() {
        return c;
    }

    // ritorna un nuovo triangolo traslato di (+dx, +dy) sul piano
    public ImmutableTriangle translate(double dx, double dy) {
        return new ImmutableTriangle(a.translate(dx, dy),
                                     b.translate(dx, dy),
                                     c.translate(dx, dy));
    }

    // ritorna un nuovo triangolo ruotato rispetto all'origine 
    // di @angle radianti, in senso antiorario
    public ImmutableTriangle rotate(double angle) {
        return new ImmutableTriangle(a.rotate(angle),
                                     b.rotate(angle),
                                     c.rotate(angle));
    }

    // ritorna il perimetro del triangolo
    public double perimeter() {
        return a.distance(b) + b.distance(c) + c.distance(a);
    }

    // ritorna l'area del triangolo
    public double area() {
        return Math.abs(0.5 * (a.getX() * (b.getY() - c.getY()) +
                               b.getX() * (c.getY() - a.getY()) +
                               c.getX() * (a.getY() - b.getY())));
    }
}
