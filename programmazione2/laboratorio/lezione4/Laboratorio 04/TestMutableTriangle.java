public class TestMutableTriangle {
    public static void main(String[] args) {
        MutableTriangle t = new MutableTriangle(new MutablePoint(15, 15),
                                                new MutablePoint(23, 30),
                                                new MutablePoint(50, 25));
        System.out.println("perimeter (80.8596) = " + t.perimeter());
        System.out.println("area      (222.5)   = " + t.area());

        t.translate(-30, -20);
        t.rotate(0.5 * Math.PI);
        System.out.println("perimeter  = " + t.perimeter());
        System.out.println("area       = " + t.area());

        System.out.println("A.x  (5)   = " + t.getA().getX());
        System.out.println("A.y  (-15) = " + t.getA().getY());
        System.out.println("B.x  (-10) = " + t.getB().getX());
        System.out.println("B.y  (-7)  = " + t.getB().getY());
        System.out.println("C.x  (-5)  = " + t.getC().getX());
        System.out.println("C.y  (20)  = " + t.getC().getY());
    }
}
