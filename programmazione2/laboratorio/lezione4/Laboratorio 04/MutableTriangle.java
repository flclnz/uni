// classe mutabile che rappresenta un triangolo su un piano cartesiano
public class MutableTriangle {
    // i tre punti (mutabili) che formano i vertici del triangolo
    private MutablePoint a;
    private MutablePoint b;
    private MutablePoint c;

    // costruttore
    public MutableTriangle(MutablePoint a, MutablePoint b, MutablePoint c) {
        assert (a != null) && (b != null) && (c != null);
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public MutablePoint getA() { 
        return a;
    }

    public MutablePoint getB() { 
        return b;
    }

    public MutablePoint getC() { 
        return c;
    }

    // trasla il triangolo di (+dx, +dy) sul piano
    public void translate(double dx, double dy) {
        a.translate(dx, dy);
        b.translate(dx, dy);
        c.translate(dx, dy);
    }

    // ruota il triangolo rispetto all'origine di @angle radianti, in senso antiorario
    public void rotate(double angle) {
        a.rotate(angle);
        b.rotate(angle);
        c.rotate(angle);
    }

    // ritorna il perimetro del triangolo
    public double perimeter() { 
        return a.distance(b) + b.distance(c) + c.distance(a); 
    }

    // ritorna l'area del triangolo
    public double area() {
        return Math.abs(0.5 * (a.getX() * (b.getY() - c.getY()) +
                               b.getX() * (c.getY() - a.getY()) +
                               c.getX() * (a.getY() - b.getY())));
    }
}
