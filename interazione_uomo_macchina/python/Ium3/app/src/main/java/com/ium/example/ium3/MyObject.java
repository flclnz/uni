package com.ium.example.ium3;

public class MyObject {
    public String nome;
    public String cognome;
    public int eta;
    public String aggiunto;

    public static MyObject[] createObjects() {
        MyObject[] ret = new MyObject[5];
        for (int i = 0;
             i < 5; i++){
            MyObject ob = new MyObject();
            ob.cognome = "COGN_" + i;
            ob.nome = "NOME_" + i;
            ob.eta = i+10;
            ob.aggiunto = "AGG"+ i;
            ret[i] =ob;
        }
        return ret;
    }
}
