package com.ium.example.ium3;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class CustomAdapter extends ArrayAdapter<MyObject> {

    LayoutInflater inflater;
    int layoutResourceId;
    MyObject data[];

    public CustomAdapter(Context context, int layoutResourceId, MyObject[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.inflater = ((Activity) context).getLayoutInflater();
        this.data = data;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View row = convertView;
        MyObject ob = data[position];
        if (row == null) {
            // cerca i widget all'interno del singolo list item
            row = inflater.inflate(layoutResourceId, parent, false);
            ((TextView) (row.findViewById(R.id.editText1))).setText(ob.nome);
            ((TextView) (row.findViewById(R.id.editText2))).setText(ob.cognome);
            ((TextView) (row.findViewById(R.id.editText3))).setText(""+ob.eta);
            ((TextView) (row.findViewById(R.id.editText4))).setText(""+ob.aggiunto);
        }
        Button btn = row.findViewById(R.id.add_btn);
        btn.setOnClickListener(v -> Toast.makeText(parent.getContext(), "CLICK: " + position, Toast.LENGTH_LONG).show());
        return row;
    }

}
