package com.ium.example.applicazione1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("mainactiviy.oncreate()", "inizio");
        setContentView(R.layout.activity_main);
    }

    public void clic1(View v) {
        Intent intento = new Intent(this,MainActivity2.class);
        startActivity(intento);
    }

    public void clic1Orig(View v) {
        // v e' la View su cui e' avvenuto l'evento
        Button b = (Button) v;
        Log.d("mainactivity.clic1()", b.getText().toString());  // mostra testo del bottone
        b.setText(R.string.CLIC1);

        // esempio di comando ritardato:
        // cambia layout aspettando 3 secondi

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                setContentView(R.layout.layout2);
            }
        }, 3000);


    }


    public void clic2(View v) {
        Log.d("mainactivity.clic2()", "click");
        setContentView(R.layout.activity_main); // torna al primo layout subito
        // mostra un Toast di esempio
        showToast();

    }

    private void showToast(){
        Context context = getApplicationContext();
        CharSequence text = "Hello toast!";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

}