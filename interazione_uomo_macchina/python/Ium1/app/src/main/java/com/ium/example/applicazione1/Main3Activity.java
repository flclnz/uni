package com.ium.example.applicazione1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class Main3Activity extends AppCompatActivity {

    EditText t;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intento = getIntent();
        String param = "";//intento.getStringExtra(MainActivity2.PARAMETRO);
        Log.d("Main3ctivity.oncreate()","parametro="+param);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        t = findViewById(R.id.editTextTextPersonName);
        t.setText("da activity 2:"+param);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public void ritorna(View v){
        Intent result = new Intent();
        String s = t.getText().toString();
        result.putExtra(MainActivity2.RESULT_MESSAGE, s);
        setResult(Activity.RESULT_OK, result);
        // se non richiamo finish() rimane Attiva
        finish();
    }
}