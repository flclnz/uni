package com.ium.example.calcola;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void operazione(View v) {
        EditText t1 = findViewById(R.id.op1);
        EditText t2 = findViewById(R.id.op2);
        TextView ris = findViewById(R.id.ris);
        String s1 = t1.getText().toString();
        String s2 = t2.getText().toString();
        int n1 = Integer.parseInt(s1);
        int n2 = Integer.parseInt(s2);
        int n3 = n1*n2;
        ris.setText(getString(R.string.risultato,n3));
    }
}