package com.ium.example.applicazione1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class Main3Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intento = getIntent();
        String param = intento.getStringExtra(MainActivity2.PARAMETRO);
        Log.d("Main3ctivity.oncreate()","parametro="+param);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
    }
}